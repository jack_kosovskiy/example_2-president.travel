filterPram:{
	directFlight: false;
	max1Transfer: true; //если непрямой рейс
	nightTransfer: true; //если непрямой рейс
	changeAiport: false; //если непрямой рейс

	minTransferTime: 35; //если непрямой рейс в минутах 
	maxTransferTime: 500; //если непрямой рейс

	forwardTimeStart: 1;
	forwardTimeEnd: 2;
	backwardTimeStart: 3; //если полет в 2 конца
	backwardTimeEnd: 4; //если полет в 2 конца

	airportForwardStart: 'SVO';
	airportBackwardEnd: 'SVO';
	airportForwardStart: 'SVO';
	airportBackwardEnd: 'SVO';

	airline: ['UT','PS','TK']
}
