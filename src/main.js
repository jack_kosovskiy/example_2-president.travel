// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue';
import store from './store';
import VueProgressBar from 'vue-progressbar';
import Vuebar from 'vuebar';
import VueNotifications from 'vue-notifications';
import miniToastr from 'mini-toastr';
import * as VueGoogleMaps from 'vue2-google-maps';
import VueLazyLoad from 'vue-lazyload'
import VueTouch from 'vue-touch'
import VueI18n from 'vue-i18n';
import VueMask from 'v-mask'
import messages from './localization/dictionary';

import App from './App';
import router from './router';

import MaskedInput from 'vue-text-mask'


Vue.component('masked-input', MaskedInput);


Vue.config.productionTip = false

/* eslint-disable no-new */

const options = {
  debug: true,
  color: '#FC4955',
  failedColor: '#C3C5CD',
  thickness: '5px',
  transition: {
    speed: '60s',
    opacity: '0.6s',
    termination: 60000
  },
  autoRevert: true,
  location: 'top',
  inverse: false
};
// If using mini-toastr, provide additional configuration
const toastTypes = {
  success: 'success',
  error: 'error',
  info: 'info',
  warn: 'warn'
};
miniToastr.init({
  types: toastTypes
})
// Here we setup messages output to `mini-toastr`
function toast({
  title,
  message,
  type,
  timeout,
  cb
}) {
  return miniToastr[type](message, title, timeout, cb)
}

// Binding for methods .success(), .error() and etc. You can specify and map your own methods here.
// Required to pipe our output to UI library (mini-toastr in example here)
// All not-specified events (types) would be piped to output in console.
const optionsNotification = {
  success: toast,
  error: toast,
  info: toast,
  warn: toast
}

// Activate plugin
Vue.use(VueNotifications, optionsNotification);
Vue.use(VueLazyLoad)
Vue.use(VueTouch, {
  name: 'v-touch'
})
Vue.use(VueGoogleMaps, {
  load: {
    key: 'AIzaSyAPS0VyMAM7_m429obp7CFMXAaSk65ooDA',
    v: '3.27',
    libraries: 'places',
  }
});
Vue.use(VueMask);
Vue.use(VueProgressBar, options);
Vue.use(VueI18n);
Vue.use(Vuebar);

Vue.mixin({
	computed: {
		currentLangCode () {
			return this.$store.state.langs.currentLang.code
		}
	}
});

Vue.directive('click-outside', {
  bind: function (el, binding, vnode) {
    el.event = function (event) {
      // here I check that click was outside the el and his childrens
      if (!(el == event.target || el.contains(event.target))) {
        // and if it did, call method provided in attribute value
        vnode.context[binding.expression](event);
      }
    };
    document.body.addEventListener('click', el.event)
  },
  unbind: function (el) {
    document.body.removeEventListener('click', el.event)
  },
});

const i18n = new VueI18n({
  locale: 'ru', // set locale
  messages, // set locale messages
})

new Vue({
  el: '#app',
  router,
  store,
  template: '<App/>',
  components: {
    App
  },
  i18n
})
