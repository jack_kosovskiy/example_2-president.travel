import albania from '../../assets/flags/svg/albania.svg';
import afghanistan from '../../assets/flags/svg/afghanistan.svg';
import antigua from '../../assets/flags/svg/antigua-and-barbuda.svg';
import andorra from '../../assets/flags/svg/andorra.svg';
import anguilla from '../../assets/flags/svg/anguilla.svg';
import armenia from '../../assets/flags/svg/armenia.svg';
import angola from '../../assets/flags/svg/angola.svg';
import argentina from '../../assets/flags/svg/argentina.svg';
import austria from '../../assets/flags/svg/austria.svg';
import australia from '../../assets/flags/svg/australia.svg';
import aruba from '../../assets/flags/svg/aruba.svg';
import azerbaijan from '../../assets/flags/svg/azerbaijan.svg';
import algeria from '../../assets/flags/svg/algeria.svg';

import bosnia from '../../assets/flags/svg/bosnia-and-herzegovina.svg';
import barbados from '../../assets/flags/svg/barbados.svg';
import bangladesh from '../../assets/flags/svg/bangladesh.svg';
import belgium from '../../assets/flags/svg/belgium.svg';
import burkina from '../../assets/flags/svg/burkina-faso.svg';
import bulgaria from '../../assets/flags/svg/bulgaria.svg';
import bahrain from '../../assets/flags/svg/bahrain.svg';
import burundi from '../../assets/flags/svg/burundi.svg';
import benin from '../../assets/flags/svg/benin.svg';
import bermuda from '../../assets/flags/svg/bermuda.svg';
import brunei from '../../assets/flags/svg/brunei.svg';
import bolivia from '../../assets/flags/svg/bolivia.svg';
import bahamas from '../../assets/flags/svg/bahamas.svg';
import botswana from '../../assets/flags/svg/botswana.svg';
import brazil from '../../assets/flags/svg/brazil.svg';
import belarus from '../../assets/flags/svg/belarus.svg';
import belize from '../../assets/flags/svg/belize.svg';

import car from '../../assets/flags/svg/central-african-republic.svg';
import cook from '../../assets/flags/svg/cook-islands.svg';
import chile from '../../assets/flags/svg/chile.svg';
import cameroon from '../../assets/flags/svg/cameroon.svg';
import china from '../../assets/flags/svg/china.svg';
import colombia from '../../assets/flags/svg/colombia.svg';
import costa from '../../assets/flags/svg/costa-rica.svg';
import cuba from '../../assets/flags/svg/cuba.svg';
import cape from '../../assets/flags/svg/cape-verde.svg';
import curacao from '../../assets/flags/svg/curacao.svg';
import cyprus from '../../assets/flags/svg/cyprus.svg';
import czech from '../../assets/flags/svg/czech-republic.svg';

import democratic from '../../assets/flags/svg/democratic-republic-of-congo.svg';
import djibouti from '../../assets/flags/svg/djibouti.svg';
import denmark from '../../assets/flags/svg/denmark.svg';
import dominica from '../../assets/flags/svg/dominica.svg';
import dr from '../../assets/flags/svg/dominican-republic.svg';


import ecuador from '../../assets/flags/svg/ecuador.svg';
import estonia from '../../assets/flags/svg/estonia.svg';
import egypt from '../../assets/flags/svg/egypt.svg';
import eritrea from '../../assets/flags/svg/eritrea.svg';

import spain from '../../assets/flags/svg/spain.svg';
import ethiopia from '../../assets/flags/svg/ethiopia.svg';
import finland from '../../assets/flags/svg/finland.svg';
import fiji from '../../assets/flags/svg/fiji.svg';
import falkland from '../../assets/flags/svg/falkland-islands.svg';
import micronesia from '../../assets/flags/svg/micronesia.svg';
import faroe from '../../assets/flags/svg/faroe-islands.svg';
import france from '../../assets/flags/svg/france.svg';
import gabon from '../../assets/flags/svg/gabon.svg';
import grenada from '../../assets/flags/svg/grenada.svg';
import georgia from '../../assets/flags/svg/georgia.svg';
import ghana from '../../assets/flags/svg/ghana.svg';
import gibraltar from '../../assets/flags/svg/gibraltar.svg';
import greenland from '../../assets/flags/svg/greenland.svg';
import gambia from '../../assets/flags/svg/gambia.svg';
import guinea from '../../assets/flags/svg/guinea.svg';
import greece from '../../assets/flags/svg/greece.svg';
import guatemala from '../../assets/flags/svg/guatemala.svg';
import guam from '../../assets/flags/svg/guam.svg';
import bissau from '../../assets/flags/svg/guinea-bissau.svg';
import guyana from '../../assets/flags/svg/guyana.svg';
import hk from '../../assets/flags/svg/hong-kong.svg';
import honduras from '../../assets/flags/svg/honduras.svg';
import croatia from '../../assets/flags/svg/croatia.svg';
import haiti from '../../assets/flags/svg/haiti.svg';
import hungary from '../../assets/flags/svg/hungary.svg';
import indonesia from '../../assets/flags/svg/indonesia.svg';
import ireland from '../../assets/flags/svg/ireland.svg';
import israel from '../../assets/flags/svg/israel.svg';
import india from '../../assets/flags/svg/india.svg';
import iraq from '../../assets/flags/svg/iraq.svg';
import iran from '../../assets/flags/svg/iran.svg';
import iceland from '../../assets/flags/svg/iceland.svg';
import italy from '../../assets/flags/svg/italy.svg';
import jamaica from '../../assets/flags/svg/jamaica.svg';
import jordan from '../../assets/flags/svg/jordan.svg';
import japan from '../../assets/flags/svg/japan.svg';
import kenya from '../../assets/flags/svg/kenya.svg';
import kyrgyzstan from '../../assets/flags/svg/kyrgyzstan.svg';
import cambodia from '../../assets/flags/svg/cambodia.svg';
import kiribati from '../../assets/flags/svg/kiribati.svg';
import comoros from '../../assets/flags/svg/comoros.svg';
import saint from '../../assets/flags/svg/saint-kitts-and-nevis.svg';
import kuwait from '../../assets/flags/svg/kuwait.svg';
import cayman from '../../assets/flags/svg/cayman-islands.svg';
import kazakhstan from '../../assets/flags/svg/kazakhstan.svg';
import laos from '../../assets/flags/svg/laos.svg';
import lebanon from '../../assets/flags/svg/lebanon.svg';
import liechtenstein from '../../assets/flags/svg/liechtenstein.svg';
import lanka from '../../assets/flags/svg/sri-lanka.svg';
import liberia from '../../assets/flags/svg/liberia.svg';
import lesotho from '../../assets/flags/svg/lesotho.svg';
import lithuania from '../../assets/flags/svg/lithuania.svg';
import luxembourg from '../../assets/flags/svg/luxembourg.svg';
import latvia from '../../assets/flags/svg/latvia.svg';
import libya from '../../assets/flags/svg/libya.svg';
import morocco from '../../assets/flags/svg/morocco.svg';
import monaco from '../../assets/flags/svg/monaco.svg';
import moldova from '../../assets/flags/svg/moldova.svg';
import montenegro from '../../assets/flags/svg/montenegro.svg';
import madagascar from '../../assets/flags/svg/madagascar.svg';
import marshall from '../../assets/flags/svg/marshall-island.svg';
import macedonia from '../../assets/flags/svg/republic-of-macedonia.svg';
import mali from '../../assets/flags/svg/mali.svg';
import myanmar from '../../assets/flags/svg/myanmar.svg';
import mongolia from '../../assets/flags/svg/mongolia.svg';
import macao from '../../assets/flags/svg/macao.svg';
import marianas from '../../assets/flags/svg/northern-marianas-islands.svg';
import martinique from '../../assets/flags/svg/martinique.svg';
import mauritania from '../../assets/flags/svg/mauritania.svg';
import montserrat from '../../assets/flags/svg/montserrat.svg';
import malta from '../../assets/flags/svg/malta.svg';
import mauritius from '../../assets/flags/svg/mauritius.svg';
import maldives from '../../assets/flags/svg/maldives.svg';
import malawi from '../../assets/flags/svg/malawi.svg';
import mexico from '../../assets/flags/svg/mexico.svg';
import malaysia from '../../assets/flags/svg/malaysia.svg';
import mozambique from '../../assets/flags/svg/mozambique.svg';
import namibia from '../../assets/flags/svg/namibia.svg';
import niger from '../../assets/flags/svg/niger.svg';
import nk from '../../assets/flags/svg/north-korea.svg';
import sk from '../../assets/flags/svg/south-korea.svg';
import norfolk from '../../assets/flags/svg/norfolk-island.svg';
import nigeria from '../../assets/flags/svg/nigeria.svg';
import nicaragua from '../../assets/flags/svg/nicaragua.svg';
import norway from '../../assets/flags/svg/norway.svg';
import nepal from '../../assets/flags/svg/nepal.svg';
import nauru from '../../assets/flags/svg/nauru.svg';
import niue from '../../assets/flags/svg/niue.svg';
import zealand from '../../assets/flags/svg/new-zealand.svg';
import oman from '../../assets/flags/svg/oman.svg';
import panama from '../../assets/flags/svg/panama.svg';
import peru from '../../assets/flags/svg/peru.svg';
import polynesia from '../../assets/flags/svg/french-polynesia.svg';
import papua from '../../assets/flags/svg/papua-new-guinea.svg';
import philippines from '../../assets/flags/svg/philippines.svg';
import pakistan from '../../assets/flags/svg/pakistan.svg';
import palestine from '../../assets/flags/svg/palestine.svg';
import portugal from '../../assets/flags/svg/portugal.svg';
import palau from '../../assets/flags/svg/palau.svg';
import paraguay from '../../assets/flags/svg/paraguay.svg';
import qatar from '../../assets/flags/svg/qatar.svg';
import romania from '../../assets/flags/svg/romania.svg';
import serbia from '../../assets/flags/svg/serbia.svg';
import rwanda from '../../assets/flags/svg/rwanda.svg';
import arabia from '../../assets/flags/svg/saudi-arabia.svg';
import solomon from '../../assets/flags/svg/solomon-islands.svg';
import seychelles from '../../assets/flags/svg/seychelles.svg';
import sudan from '../../assets/flags/svg/sudan.svg';
import sweden from '../../assets/flags/svg/sweden.svg';
import singapore from '../../assets/flags/svg/singapore.svg';
import slovakia from '../../assets/flags/svg/slovakia.svg';
import slovenia from '../../assets/flags/svg/slovenia.svg';
import sierra from '../../assets/flags/svg/sierra-leone.svg';
import marino from '../../assets/flags/svg/san-marino.svg';
import senegal from '../../assets/flags/svg/senegal.svg';
import somalia from '../../assets/flags/svg/somalia.svg';
import suriname from '../../assets/flags/svg/suriname.svg';
import ss from '../../assets/flags/svg/south-sudan.svg';
import sao from '../../assets/flags/svg/sao-tome-and-principe.svg';
import salvador from '../../assets/flags/svg/salvador.svg';
import maarten from '../../assets/flags/svg/sint-maarten.svg';
import syria from '../../assets/flags/svg/syria.svg';
import swaziland from '../../assets/flags/svg/swaziland.svg';
import turks from '../../assets/flags/svg/turks-and-caicos.svg';
import chad from '../../assets/flags/svg/chad.svg';
import togo from '../../assets/flags/svg/togo.svg';
import thailand from '../../assets/flags/svg/thailand.svg';
import tajikistan from '../../assets/flags/svg/tajikistan.svg';
import tokelau from '../../assets/flags/svg/tokelau.svg';
import timor from '../../assets/flags/svg/east-timor.svg';
import turkmenistan from '../../assets/flags/svg/turkmenistan.svg';
import tunisia from '../../assets/flags/svg/tunisia.svg';
import tonga from '../../assets/flags/svg/tonga.svg';
import turkey from '../../assets/flags/svg/turkey.svg';
import trinidad from '../../assets/flags/svg/trinidad-and-tobago.svg';
import tuvalu from '../../assets/flags/svg/tuvalu.svg';
import taiwan from '../../assets/flags/svg/taiwan.svg';
import tanzania from '../../assets/flags/svg/tanzania.svg';
import uganda from '../../assets/flags/svg/uganda.svg';
import kingdom from '../../assets/flags/svg/united-kingdom.svg';
import uruguay from '../../assets/flags/svg/uruguay.svg';
import uzbekistn from '../../assets/flags/svg/uzbekistn.svg';
import vatican from '../../assets/flags/svg/vatican-city.svg';
import venezuela from '../../assets/flags/svg/venezuela.svg';
import virgin from '../../assets/flags/svg/british-virgin-islands.svg';
import vietnam from '../../assets/flags/svg/vietnam.svg';
import vanuatu from '../../assets/flags/svg/vanuatu.svg';
import yemen from '../../assets/flags/svg/yemen.svg';
import sa from '../../assets/flags/svg/south-africa.svg';
import zambia from '../../assets/flags/svg/zambia.svg';
import zimbabwe from '../../assets/flags/svg/zimbabwe.svg';
// import ecuador from '../../assets/flags/svg/';
// import ecuador from '../../assets/flags/svg/';
// import ecuador from '../../assets/flags/svg/';
// import ecuador from '../../assets/flags/svg/';import ecuador from '../../assets/flags/svg/';
// import ecuador from '../../assets/flags/svg/';
// import ecuador from '../../assets/flags/svg/';
// import ecuador from '../../assets/flags/svg/';
// import ecuador from '../../assets/flags/svg/';
// import ecuador from '../../assets/flags/svg/';
// import ecuador from '../../assets/flags/svg/';
// import ecuador from '../../assets/flags/svg/';
// import ecuador from '../../assets/flags/svg/';
// import ecuador from '../../assets/flags/svg/';
// import ecuador from '../../assets/flags/svg/';
// import ecuador from '../../assets/flags/svg/';
// import ecuador from '../../assets/flags/svg/';
// import ecuador from '../../assets/flags/svg/';
// import ecuador from '../../assets/flags/svg/';
// import ecuador from '../../assets/flags/svg/';
// import ecuador from '../../assets/flags/svg/';
// import ecuador from '../../assets/flags/svg/';
// import ecuador from '../../assets/flags/svg/';
// import ecuador from '../../assets/flags/svg/';
// import ecuador from '../../assets/flags/svg/';
// import ecuador from '../../assets/flags/svg/';
// import ecuador from '../../assets/flags/svg/';
// import ecuador from '../../assets/flags/svg/';
// import ecuador from '../../assets/flags/svg/';
// import ecuador from '../../assets/flags/svg/';
// import ecuador from '../../assets/flags/svg/';
// import ecuador from '../../assets/flags/svg/';

import switzerland from '../../assets/flags/svg/switzerland.svg';


import germany from '../../assets/flags/svg/germany.svg';



import netherlands from '../../assets/flags/svg/netherlands.svg';


import uae from '../../assets/flags/svg/united-arab-emirates.svg';
import samoa from '../../assets/flags/svg/samoa.svg';
import russia from '../../assets/flags/svg/russia.svg';
import usa from '../../assets/flags/svg/united-states-of-america.svg';
import canada from '../../assets/flags/svg/canada.svg';

import ukraine from '../../assets/flags/svg/ukraine.svg';

export default {
  ukraine,
  russia,
  albania,
  afghanistan,
  antigua,
  algeria,
  usa,
  uae,
  samoa,
  netherlands,
  andorra,
  anguilla,
  armenia,
  argentina,
  angola,
  austria,
  australia,
  aruba,
  azerbaijan,

  bosnia,
  barbados,
  bangladesh,
  belgium,
  burkina,
  bulgaria,
  bahrain,
  burundi,
  benin,
  bermuda,
  brunei,
  bolivia,
  brazil,
  bahamas,
  botswana,
  belarus,
  belize,

  democratic,
  djibouti,
  denmark,
  dominica,
  dr,

  canada,
  car,
  cook,
  chile,
  cameroon,
  china,
  colombia,
  costa,
  cuba,
  cape,
  curacao,
  cyprus,
  czech,
  cambodia,

  ecuador,
  estonia,
  egypt,
  eritrea,

  germany,
  spain,
  ethiopia,
  finland,
  fiji,
  falkland,
  faroe,
  france,
  gabon,
  grenada,
  georgia,
  ghana,
  gibraltar,
  greenland,
  gambia,
  guinea,
  greece,
  guatemala,
  guam,
  bissau,
  guyana,
  hk,
  honduras,
  croatia,
  haiti,
  hungary,
  indonesia,
  ireland,
  israel,
  india,
  iraq,
  iran,
  iceland,
  italy,
  jamaica,
  jordan,
  japan,
  kenya,
  kyrgyzstan,
  micronesia,
  kiribati,
  comoros,
  saint,
  kuwait,
  cayman,
  kazakhstan,
  laos,
  lebanon,
  liechtenstein,
  lanka,
  liberia,
  lesotho,
  lithuania,
  luxembourg,
  latvia,
  libya,
  morocco,
  monaco,
  moldova,
  montenegro,
  madagascar,
  marshall,
  macedonia,
  mali,
  myanmar,
  mongolia,
  macao,
  marianas,
  martinique,
  mauritania,
  montserrat,
  malta,
  mauritius,
  maldives,
  malawi,
  mexico,
  malaysia,
  mozambique,
  namibia,
  niger,
  nk,
  sk,
  norfolk,
  nigeria,
  nicaragua,
  norway,
  nepal,
  nauru,
  niue,
  zealand,
  oman,
  panama,
  peru,
  polynesia,
  papua,
  philippines,
  pakistan,
  palestine,
  portugal,
  palau,
  paraguay,
  qatar,
  romania,
  serbia,
  rwanda,
  arabia,
  solomon,
  seychelles,
  sudan,
  sweden,
  singapore,
  slovakia,
  slovenia,
  somalia,
  ss,
  sierra,
  marino,
  senegal,
  suriname,
  sao,
  salvador,
  maarten,
  syria,
  swaziland,
  turks,
  chad,
  togo,
  thailand,
  tajikistan,
  tokelau,
  timor,
  turkmenistan,
  tunisia,
  tonga,
  turkey,
  trinidad,
  tuvalu,
  taiwan,
  tanzania,
  uganda,
  kingdom,
  uruguay,
  uzbekistn,
  vatican,
  venezuela,
  virgin,
  vietnam,
  vanuatu,
  yemen,
  sa,
  zambia,
  zimbabwe,
  switzerland
}
