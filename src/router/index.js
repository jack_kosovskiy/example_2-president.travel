import Vue from "vue";
import Router from "vue-router";
import Dashboard from "@/components/Dashboard";
import TicketSelection from "@/components/TicketSelection";
import Hotels from "@/components/Hotels";
import HotelPage from "@/components/hotels/HotelPage";
import PurchaseRegistration from "@/components/purchase/PurchaseRegistration";
import PurchaseHotel from "@/components/purchase/PurchaseHotel";
import Agreements from "@/components/Agreements";
import HotelResult from "@/components/purchase/HotelResult"
import TicketResult from "../components/TicketResult"
import TicketGateLoader from "../components/ticketGateLoader"

//auth
import LoginUser from "@/components/auth/AuthLoginUser";
import LoginCorp from "@/components/auth/AuthLoginCorporate";
import ForgotPassUser from "@/components/auth/AuthForgotPassUser";
import ForgotPassCorporate from "@/components/auth/AuthForgotPassCorporate";
import RegUser from "@/components/auth/RegUser-1";
import RegUser2 from "@/components/auth/RegUser-2";
import RegUser3 from "@/components/auth/RegUser-3";
import RegCorp1 from "@/components/auth/RegCorporate-1";
import RegCorp2 from "@/components/auth/RegCorporate-2";
import RegCorp3 from "@/components/auth/RegCorporate-3";
import RegCorp4 from "@/components/auth/RegCorporate-4";
import RegCorp5 from "@/components/auth/RegCorporate-5";
import RegCorp6 from "@/components/auth/RegCorporate-6";
import RegCorp7 from "@/components/auth/RegCorporate-7";

Vue.use(Router);

const router = new Router({
	routes: [
		{
			path: "/",
			name: "Dashboard",
			component: Dashboard
		},
		{
			path: "/gate",
			name: "gate",
			component: TicketGateLoader
		},
		{
			path: "/hotel-search",
			name: "HotelsSearch",
			component: Dashboard
		},
		{
			path: "/hotels",
			name: "Hotels",
			component: Hotels
		},
		{
			path: "/current-hotel/:id",
			name: "CurrentHotel",
			component: HotelPage
		},
		{
			path: "/ticket-selection",
			name: "TicketSelection",
			component: TicketSelection
		},
		{
			path: "/purchase",
			name: "PurchaseRegistration",
			component: PurchaseRegistration
		},
		{
			path: "/purchase-hotel",
			name: "PurchaseHotel",
			component: PurchaseHotel
		},
		{
			path: "/hotel/:book",
			name: "HotelResult",
			component: HotelResult
		},
		{
			path: "/ticket/:book",
			name: "TicketBook",
			component: TicketResult
		},

		//незалогиненый пользователь
		{
			path: "/agreements",
			name: "Agreements",
			component: Agreements
		},
		{
			path: "/login-user",
			name: "LogiUser",
			component: LoginUser
		},
		{
			path: "/forgot-pass-user",
			name: "ForgotPassUser",
			component: ForgotPassUser
		},
		{
			path: "/forgot-pass-corporate",
			name: "ForgotPassCorporate",
			component: ForgotPassCorporate
		},
		{
			path: "/login-corp",
			name: "LoginCorp",
			component: LoginCorp
		},
		{
			path: "/reg-user",
			name: "RegUser",
			component: RegUser
		},
		{
			path: "/reg-user/step2",
			name: "RegUser2",
			component: RegUser2
		},
		{
			path: "/reg-user/step3",
			name: "RegUser3",
			component: RegUser3
		},
		{
			path: "/reg-corp",
			name: "RegCorp1",
			component: RegCorp1
		},
		{
			path: "/reg-corp/step2",
			name: "RegCorp2",
			component: RegCorp2
		},
		{
			path: "/reg-corp/step3",
			name: "RegCorp3",
			component: RegCorp3
		},
		{
			path: "/reg-corp/step4",
			name: "RegCorp4",
			component: RegCorp4
		},
		{
			path: "/reg-corp/step5",
			name: "RegCorp5",
			component: RegCorp5
		},
		{
			path: "/reg-corp/step6",
			name: "RegCorp6",
			component: RegCorp6
		},
		{
			path: "/reg-corp/step7",
			name: "RegCorp7",
			component: RegCorp7
		},
		{
			path: "*",
			name: "/",
			component: Dashboard
		}
	],
});
router.afterEach((to, from) => {
	document.body.scrollTop = document.documentElement.scrollTop = 0;
})
router.beforeEach((to, from, next) => {
	// console.log(to)
	// console.log(from)
	if (localStorage.getItem("President_Travel_token") == null) {
		//пользователь НЕ залогинен
		if (
			to.name == "Agreements" ||
			to.name == "LogiUser" ||
			to.name == "ForgotPassUser" ||
			to.name == "ForgotPassCorporate" ||
			to.name == "LoginCorp" ||
			to.name == "RegUser" ||
			to.name == "RegUser2" ||
			to.name == "RegUser3" ||
			to.name == "RegCorp1" ||
			to.name == "RegCorp2" ||
			to.name == "RegCorp3" ||
			to.name == "RegCorp4" ||
			to.name == "RegCorp5" ||
			to.name == "RegCorp6" ||
			to.name == "RegCorp7" ||
			to.name == "TicketSelection"
		) {
			next();
		} else {
			next({
				path: "/login-user"
			});
		}
	} else {
		//пользователь залогинен
		next();
	}
});

export default router;
