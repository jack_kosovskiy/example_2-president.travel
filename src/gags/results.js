export default [
  {
    id:1,
    airlineID:1,
    fromCity: 'Москва',
    toCity: 'Лондон',
    fromAirportID: 1,
    fromDate: '2017-06-6T18:25:43.511Z',
    backDate: '2017-06-10T18:25:43.511Z',
    toAirportID: 6,
    classID:1,
    transfers:[],
    filghtsTo:[{
      id: 1,
      fromTime: '2017-06-6T13:25:43.511Z',
      toTime: '2017-06-6T24:21:43.511Z',
      cost: 30245,
      transerParameters: ['1', '0'],
      transferDuration: 40,
      currencyID: 1
    },{
      id: 2,
      fromTime: '2017-06-6T3:25:43.511Z',
      toTime: '2017-06-6T23:25:43.511Z',
      cost: 31145,
      transerParameters: ['1', '2'],
      transferDuration: 123,
      currencyID: 1
    },{
      id: 3,
      fromTime: '2017-06-6T18:01:43.511Z',
      toTime: '2017-06-6T18:02:43.511Z',
      cost: 31241,
      transferDuration: 40,
      transerParameters: ['1', '0'],
      currencyID: 1
    },{
      id: 4,
      fromTime: '2017-06-6T18:25:43.511Z',
      toTime: '2017-06-6T23:25:43.511Z',
      cost: 31240,
      transferDuration: 140,
      transerParameters: ['1', '0'],
      currencyID: 1
    }],
    flightsBack:[{
      id: 1,
      fromTime: '2017-06-6T12:25:43.511Z',
      toTime: '2017-06-6T23:25:43.511Z',
      cost: 3345,
      transferDuration: 420,
      transerParameters: ['1', '2'],
      currencyID: 1
    },{
      id: 2,
      fromTime: '2017-06-6T18:25:43.511Z',
      toTime: '2017-06-6T23:25:43.511Z',
      cost: 13245,
      transferDuration: 45,
      transerParameters: ['3', '0'],
      currencyID: 1
    },{
      id: 3,
      fromTime: '2017-06-6T18:25:43.511Z',
      toTime: '2017-06-6T23:25:43.511Z',
      cost: 19245,
      transferDuration: 60,
      transerParameters: ['1', '3'],
      currencyID: 1
    },{
      id: 4,
      fromTime: '2017-06-6T18:25:43.511Z',
      toTime: '2017-06-6T23:25:43.511Z',
      cost: 31245,
      transerParameters: ['0', '2'],
      transferDuration: 70,
      currencyID: 1
    }]
  },{
    id:2,
    fromCity: 'Москва',
    toCity: 'Лондон',
    airlineID:2,
    fromAirportID: 1,
    fromDate: '2017-06-6T18:25:43.511Z',
    backDate: '2017-06-10T18:25:43.511Z',
    classID:1,
    toAirportID: 7,
    transfers:[],
    filghtsTo:[{
      id: 1,
      fromTime: '2017-06-6T18:25:43.511Z',
      toTime: '2017-06-6T23:25:43.511Z',
      cost: 31245,
      transerParameters: ['1', '2'],
      transferDuration: 140,
      currencyID: 1
    },{
      id: 2,
      fromTime: '2017-06-6T18:25:43.511Z',
      toTime: '2017-06-6T23:25:43.511Z',
      cost: 31245,
      transerParameters: ['2', '1'],
      transferDuration: 440,
      currencyID: 1
    },{
      id: 3,
      fromTime: '2017-06-6T18:25:43.511Z',
      toTime: '2017-06-6T23:25:43.511Z',
      cost: 31245,
      transerParameters: ['1', '2'],
      transferDuration: 450,
      currencyID: 1
    },{
      id: 4,
      fromTime: '2017-06-6T18:25:43.511Z',
      toTime: '2017-06-6T23:25:43.511Z',
      cost: 31245,
      transerParameters: ['0'],
      transferDuration: 450,
      currencyID: 1
    }],
    flightsBack:[{
      id: 1,
      fromTime: '2017-06-6T18:25:43.511Z',
      toTime: '2017-06-6T23:25:43.511Z',
      cost: 31245,
      transerParameters: ['1', '0'],
      transferDuration: 410,
      currencyID: 1
    },{
      id: 2,
      fromTime: '2017-06-6T18:25:43.511Z',
      toTime: '2017-06-6T23:25:43.511Z',
      cost: 31245,
      transerParameters: ['1'],
      transferDuration: 30,
      currencyID: 1
    },{
      id: 3,
      fromTime: '2017-06-6T18:25:43.511Z',
      toTime: '2017-06-6T23:25:43.511Z',
      cost: 31245,
      transerParameters: ['1', '0'],
      transferDuration: 450,
      currencyID: 1
    },{
      id: 4,
      fromTime: '2017-06-6T18:25:43.511Z',
      toTime: '2017-06-6T23:25:43.511Z',
      cost: 31245,
      transerParameters: ['3'],
      transferDuration: 100,
      currencyID: 1
    }]
  }

]
