export default [{
    "id": 1,
    "previousTripID": null,
    "userID": 1,
    "from": "MOW",
    "to": "LON",
    "time": "2017-10-14T18:25:43.511Z",
    "currencyID": 1,
    "cost": 62300,
    "type": "flight",
    "cityNameFrom": "Москва",
    "cityNameTo": "Мюнхен"
  },
  {
    "id": 2,
    "previousTripID": 1,
    "userID": 1,
    "from": "LON",
    "to": "MOW",
    "time": "2017-10-14T18:25:43.511Z",
    "currencyID": 1,
    "cost": 62300,
    "type": "flight",
    "cityNameFrom": "Москва",
    "cityNameTo": "Мюнхен"
  },
  {
    "id": 3,
    "previousTripID": null,
    "userID": 1,
    "from": "LON",
    "to": "MOW",
    "time": "2017-10-14T18:25:43.511Z",
    "currencyID": 1,
    "cost": 62300,
    "type": "flight",
    "cityNameFrom": "Москва",
    "cityNameTo": "Мюнхен"
  },
  {
    "id": 4,
    "previousTripID": null,
    "userID": 1,
    "from": "MOW",
    "to": "LON",
    "time": "2017-10-22T18:25:43.511Z",
    "currencyID": 1,
    "cost": 62300,
    "type": "flight",
    "cityNameFrom": "Москва",
    "cityNameTo": "Мюнхен"
  },
  {
    "id": 5,
    "previousTripID": null,
    "userID": 1,
    "from": "MOW",
    "to": "LON",
    "time": "2017-10-30T18:25:43.511Z",
    "currencyID": 1,
    "cost": 62300,
    "type": "flight",
    "cityNameFrom": "Москва",
    "cityNameTo": "Мюнхен"
  },
  {
    "id": 6,
    "previousTripID": null,
    "userID": 1,
    "from": "MOW",
    "to": "LON",
    "time": "2017-10-24T18:25:43.511Z",
    "currencyID": 1,
    "cost": 62300,
    "type": "flight",
    "cityNameFrom": "Москва",
    "cityNameTo": "Мюнхен"
  },
  {
    "id": 7,
    "previousTripID": null,
    "userID": 1,
    "from": "MOW",
    "to": "LON",
    "time": "2017-10-28T18:25:43.511Z",
    "currencyID": 1,
    "cost": 62300,
    "type": "flight",
    "cityNameFrom": "Москва",
    "cityNameTo": "Мюнхен"
  }
]

// export default [{
//     "id": 1,
//     "previousTripID": null,
//     "userID": 1,
//     "fromAirportID": 1,
//     "toAirportID": 2,
//     "time": "2017-06-1T18:25:43.511Z",
//     "currencyID": 1,
//     "cost": 62300
//   },
//   {
//     "id": 2,
//     "previousTripID": 1,
//     "userID": 1,
//     "fromAirportID": 2,
//     "toAirportID": 1,
//     "time": "2017-06-6T18:25:43.511Z",
//     "currencyID": 1,
//     "cost": 62300
//   },
//   {
//     "id": 3,
//     "previousTripID": null,
//     "userID": 1,
//     "fromAirportID": 2,
//     "toAirportID": 1,
//     "time": "2017-06-26T18:25:43.511Z",
//     "currencyID": 1,
//     "cost": 62300
//   },
//   {
//     "id": 4,
//     "previousTripID": null,
//     "userID": 1,
//     "fromAirportID": 1,
//     "toAirportID": 2,
//     "time": "2017-06-27T18:25:43.511Z",
//     "currencyID": 1,
//     "cost": 62300
//   },
//   {
//     "id": 5,
//     "previousTripID": null,
//     "userID": 1,
//     "fromAirportID": 1,
//     "toAirportID": 2,
//     "time": "2017-06-8T18:25:43.511Z",
//     "currencyID": 1,
//     "cost": 62300
//   },
//   {
//     "id": 6,
//     "previousTripID": null,
//     "userID": 1,
//     "fromAirportID": 1,
//     "toAirportID": 2,
//     "time": "2017-06-7T18:25:43.511Z",
//     "currencyID": 1,
//     "cost": 62300
//   },
//   {
//     "id": 7,
//     "previousTripID": null,
//     "userID": 1,
//     "fromAirportID": 1,
//     "toAirportID": 2,
//     "time": "2017-06-7T18:25:43.511Z",
//     "currencyID": 1,
//     "cost": 62300
//   }
// ]
