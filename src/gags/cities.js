export default [{
    "id": 1,
    "countryID": 2,
    "title": "Москва",
    "country": "Россия",
    "abbreviation": "MOW"
  },
  {
    "id": 2,
    "countryID": 2,
    "title": "Сочи",
    "country": "Россия",
    "abbreviation": "AER"
  },
  {
    "id": 3,
    "countryID": 2,
    "title": "Екатеринбург",
    "country": "Россия",
    "abbreviation": "SVX"
  },
  {
    "id": 4,
    "countryID": 2,
    "title": "Самара",
    "country": "Россия",
    "abbreviation": "SVX"
  },
  {
    "id": 5,
    "countryID": 2,
    "title": "Новосибирск",
    "country": "Россия",
    "abbreviation": "OVB"
  },
  {
    "id": 6,
    "countryID": 2,
    "title": "Калининград",
    "country": "Россия",
    "abbreviation": "KGD"
  },
  {
    "id": 7,
    "countryID": 1,
    "title": "Лондон",
    "country": "Англия",
    "abbreviation": "LON"
  },
  {
    "id": 8,
    "countryID": 3,
    "title": "Лонгйир",
    "country": "Норвегия",
    "abbreviation": "LYR"
  },
  {
    "id": 9,
    "countryID": 4,
    "title": "Лонсестон",
    "country": "Австралия",
    "abbreviation": "LST"
  },
  {
    "id": 10,
    "countryID": 5,
    "title": "Лондрина",
    "country": "Бразилия",
    "abbreviation": "LDB"
  },
  {
    "id": 11,
    "countryID": 6,
    "title": "Лонг-Ака",
    "country": "Малайзия",
    "abbreviation": "LKH"
  }
]

// export default [{
//     "id": 1,
//     "countryID": 2,
//     "title": "Москва",
//     "abbreviation": "MOW"
//   },
//   {
//     "id": 2,
//     "countryID": 2,
//     "title": "Сочи",
//     "abbreviation": "AER"
//   },
//   {
//     "id": 3,
//     "countryID": 2,
//     "title": "Екатеринбург",
//     "abbreviation": "SVX"
//   },
//   {
//     "id": 4,
//     "countryID": 2,
//     "title": "Самара",
//     "abbreviation": "SVX"
//   },
//   {
//     "id": 5,
//     "countryID": 2,
//     "title": "Новосибирск",
//     "abbreviation": "OVB"
//   },
//   {
//     "id": 6,
//     "countryID": 2,
//     "title": "Калининград",
//     "abbreviation": "KGD"
//   },
//   {
//     "id": 7,
//     "countryID": 1,
//     "title": "Лондон",
//     "abbreviation": "LON"
//   },
//   {
//     "id": 8,
//     "countryID": 3,
//     "title": "Лонгйир",
//     "abbreviation": "LYR"
//   },
//   {
//     "id": 9,
//     "countryID": 4,
//     "title": "Лонсестон",
//     "abbreviation": "LST"
//   },
//   {
//     "id": 10,
//     "countryID": 5,
//     "title": "Лондрина",
//     "abbreviation": "LDB"
//   },
//   {
//     "id": 11,
//     "countryID": 6,
//     "title": "Лонг-Ака",
//     "abbreviation": "LKH"
//   }
// ]
