export default [
    {
        "id": 1,
        "title": "Рубль",
        "symbol": "&#8381;"
    },
    {
        "id": 2,
        "title": "Dollar",
        "symbol": "&#36;"
    },
    {
        "id": 3,
        "title": "Euro",
        "symbol": "&euro;"
    },
    {
        "id": 4,
        "title": "Pound",
        "symbol": "&pound;"
    },
    {
        "id": 5,
        "title": "元",
        "symbol": "&#65509;"
    }
]
