export default [
    {
        "id": 1,
        "firstName": "Vasyl",
        "lastName": "Vaechkin",
        "birthDate": "11.01.1973",
        "gender": "male",
        "country": "Russia",
        "city": "Moscow",
        "passportSeraialNumber": "45 35 123465",
        "email": "User@gmail.com",
        "phoneNumber": "+7 (000) 000-00-00",
        "bonusCardNumber": "0000 0000 0000 0000"
    },
    {
        "id": 2,
        "firstName": "Anna",
        "lastName": "Vaechkin",
        "birthDate": "12.03.1999",
        "gender": "male",
        "country": "Russia",
        "city": "Moscow",
        "passportSeraialNumber": "45 35 123465",
        "email": "User@gmail.com",
        "phoneNumber": "+7 (000) 000-00-00",
        "bonusCardNumber": "0000 0000 0000 0000"
    },
    {
        "id": 3,
        "firstName": "Zipa",
        "lastName": "Lipa",
        "birthDate": "18.05.1996",
        "gender": "male",
        "country": "Russia",
        "city": "Moscow",
        "passportSeraialNumber": "45 35 123465",
        "email": "User@gmail.com",
        "phoneNumber": "+7 (000) 000-00-00",
        "bonusCardNumber": "0000 0000 0000 0000"
    }
    ,{
        "id": 4,
        "firstName": "Zupa",
        "lastName": "Lupa",
        "birthDate": "18.12.1994",
        "gender": "male",
        "country": "Russia",
        "city": "Moscow",
        "passportSeraialNumber": "45 35 123465",
        "email": "User@gmail.com",
        "phoneNumber": "+7 (000) 000-00-00",
        "bonusCardNumber": "0000 0000 0000 0000"
    }
]

// export default [
//     {
//         "id": 1
//         "firstName": "Vasy",
//         "lastName": "Vaechkin",
//         "birthDate": "1990-05-26T0:0:0Z",
//         "gender": "male",
//         "countryID": 2,
//         "passportSeraialNumber": "45 35 123465",
//         "email": "User@gmail.com",
//         "phoneNumber": "+7 (000) 000-00-00",
//         "bonusCardNumber": "0000 0000 0000 0000"
//     },
//     {
//         "id": 2
//         "firstName": "Vasy",
//         "lastName": "Vaechkin",
//         "birthDate": "1990-05-26T0:0:0Z",
//         "gender": "male",
//         "countryID": 2,
//         "passportSeraialNumber": "45 35 123465",
//         "email": "User@gmail.com",
//         "phoneNumber": "+7 (000) 000-00-00",
//         "bonusCardNumber": "0000 0000 0000 0000"
//     },
//     {
//         "id": 3
//         "firstName": "Vasy",
//         "lastName": "Vaechkin",
//         "birthDate": "1990-05-26T0:0:0Z",
//         "gender": "male",
//         "countryID": 2,
//         "passportSeraialNumber": "45 35 123465",
//         "email": "User@gmail.com",
//         "phoneNumber": "+7 (000) 000-00-00",
//         "bonusCardNumber": "0000 0000 0000 0000"
//     }
//     ,{
//         "id": 4
//         "firstName": "Vasy",
//         "lastName": "Vaechkin",
//         "birthDate": "1990-05-26T0:0:0Z",
//         "gender": "male",
//         "countryID": 2,
//         "passportSeraialNumber": "45 35 123465",
//         "email": "User@gmail.com",
//         "phoneNumber": "+7 (000) 000-00-00",
//         "bonusCardNumber": "0000 0000 0000 0000"
//     }
// ]
