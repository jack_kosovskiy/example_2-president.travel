export default function(price){
	return price.replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ')
}