export default function(time){
    var time = time.split('T')[1].split(':'),
      hours = +time[0],
      minutes = +time[1];
      if(hours<10){
        hours='0'+hours;
      }
      if(minutes<10){
        minutes = '0'+minutes
      }
      return hours+':'+minutes+' '
}
