export default function(dt, code){
    var date = new Date(dt);
    var day = date.getDate();
    var months = [];
    if(code == 'en'){
      months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December']
    }else{
        months = ['Января', 'Февраля', 'Марта', 'Апреля', 'Мая', 'Июня', 'Июля', 'Августа', 'Сентября', 'Октября', 'Ноября', 'Декабря']
    }
    return day+' '+months[date.getMonth()]
}
