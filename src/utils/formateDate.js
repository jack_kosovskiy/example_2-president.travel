export default function(date){
  var date = date.split('T')[0].split('-'),
      year = +date[0],
      month = +date[1],
      day = +date[2];
      if(day<10){
        day = '0'+day
      }
      if(month<10){
        month = '0'+month
      }
    return day+'.'+month+'.'+year
}
