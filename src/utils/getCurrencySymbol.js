export default function(id, currencies){
  var index = currencies.length;

  while(index--){
    if(currencies[index]['id'] == id){
      return currencies[index]
    }
  }
}
