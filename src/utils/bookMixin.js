import _ from "lodash";
import {API} from "../api";
import VueNotifications from "vue-notifications";
export default {
	data () {
		return {
			data: {},
			loading: true,
			responseErrorMessage: ""
		}
	},
	computed: {
		isDataEmpty () {
			return _.isEmpty(this.data)
		},
		defaultErrorMessage () {
			return this.$t("errors.dataFetch", this.currentLangCode);
		},
		dayLabel() {
			return this.$t("main.daylabel", this.currentLangCode);
		},
		monthLabel() {
			return this.$t("main.monthsLabels", this.currentLangCode);
		}
	},
	methods: {
		formatDate(d) {
			if (!d) return "";
			var date = new Date(d);
			d = date.getDate();
			var day = date.getDay();
			var m = date.getMonth();
			var y = date.getFullYear();
			if (this.currentLangCode === "en") return `${this.dayLabel[day]}, ${this.monthLabel[m]} ${d}, ${y}`;
			else return `${this.dayLabel[day]}, ${d} ${this.monthLabel[m]}, ${y}`;
		},
		formatTime (time) {
			if (!time) return "";
			if (this.currentLangCode === "en") {
				let hours = Number(time.split(":")[0]);
				let minutes = time.split(":")[1];
				let meridian = hours >= 12 ? "PM" : "AM";
				if (hours === 0) hours = 12;
				hours = String(hours).length === 1 ? `0${hours}` : hours;
				return `${hours}:${minutes}${meridian}`
			} else return time
		},
		formatPrice (price) {
			if (!price) return "";
			const symbol = this.data.prices.display_price_symbol;
			let stringPrice = Number(price).toFixed(2);
			let stringParts = stringPrice.split(".");
			let firstPart = stringParts[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, " ");
			let secondPart = stringParts[1];
			return this.data.prices.display_price_currency === "USD" ? `${symbol} ${firstPart}.${secondPart}` : `${firstPart}.${secondPart} ${symbol}`
		},
		formatDateTime (time, date) {
			return this.currentLangCode === "en"
				? `At ${this.formatTime(time)} ${this.formatDate(date)}`
				: `${this.formatDate(date)}, в ${this.formatTime(time)}`
		},
		loadData () {
			this.data = {};
			this.loading = true;
			API.orderStatus({id: this.$route.params.book})
				.then(response => {
					if (response.data.status === "success") this.data = response.data.data;
					else VueNotifications.error({
						message: this.$t("errors.dataFetch", this.currentLangCode)
					});
				})
				.catch(({response}) => {
					if (response.data.error) this.responseErrorMessage = response.data.error;
					else VueNotifications.error({message: this.$t("errors.unknownError", this.currentLangCode)});
				})
				.finally(() => this.loading = false)
		}
	},
	watch: {
		"$store.state.currencies.selectedCurrency": {
			deep: true,
			immediate: true,
			handler () {
				this.loadData()
			}
		}
	},
	mounted () {
		this.$emit("hideSearchbar");
	},
	beforeDestroy() {
		this.$emit("showSearchbar");
	},
}
