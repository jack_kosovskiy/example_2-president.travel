export default function (query) {
	let arrQuery = [];
	for (let key of Object.keys(query)) {
		let value = query[key];
		if (value) arrQuery.push(`${encodeURIComponent(key)}=${encodeURIComponent(value)}`)
	}
	return arrQuery.join('&')
}
