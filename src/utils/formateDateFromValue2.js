export default function(date){
  var date = new Date(date),
      year = +date.getFullYear(),
      month = +date.getMonth() +1,
      day = +date.getDate();
      if(day<10){
        day = '0'+day
      }
      if(month<10){
        month = '0'+month
      }
  return day+'-'+month+'-'+year
}
