export default function(date){
  var date = date.split('T'),
      time = date[1].split(':'),
      date = date [0].split('-'),
      year = +date[0],
      month = +date[1]-1,
      day = +date[2],
      hours = +time[0],
      minutes = +time[1],
      seconds = +time[2].split('.')[0];
      return new Date(year, month, day, hours, minutes, seconds)
}
