/**
 * Function for decline russian or eng word by some number
 * @param number
 * @param declines: 2 (or 3 for russian) words can be presented as Array or list of parameters
 * @param:shadow lang: at this time just toggle russian decline
 * @returns declined string (or input if not valid)
 */
export default function (number, ...declines) {
	let lang = declines.pop();
	if (Array.isArray(declines[0])) declines = declines[0];
	else if (!(lang === "ru" && declines.length === 3) && declines !== 2) return declines[0];
	number = String(number);
	if (lang === "ru") {
		// const firstDecline = ["1"];
		const secondDecline = ["2", "3", "4"];
		const thirdDecline = ["5", "6", "7", "8", "9", "0", "11", "12", "13", "14", "15", "16", "17", "18", "19"];
		if (thirdDecline.some(numberEnd => number.endsWith(numberEnd))) return declines[2];
		else if (secondDecline.some(numberEnd => number.endsWith(numberEnd))) return declines[1];
		else return declines[0];
	} else {
		if (number.endsWith("1")) return declines[0];
		else return declines[1];
	}
}
