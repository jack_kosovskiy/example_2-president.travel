import getDecimal from './getdecimal';
export default function (cost, currency) {
  var residue = getDecimal(cost),
    cost = ('' + Math.floor(cost)).split(''),
    index = cost.length,
    result = [];
  var temp = 1;
  while (index--) {

    if (temp % 3 == 0) {
      result.unshift(' ' + cost[index])
      temp = 0;
    } else {
      result.unshift('' + cost[index])
    }
    temp += 1;


  }
  if (residue !== 0) {
    result[result.length - 1] = +result[result.length - 1] + residue
  }
  return result.join('') + ' '
}
