import formateDate from './formateDate';
import formateTime from './formateTime';
export default function(fromTime, toTime){
  var fromDate= new Date(formateDate(fromTime)),
      toDate = new Date(formateDate(toTime)),
      fromTime = formateTime(fromTime).split(':'),
      toTime = formateTime(toTime).split(':');
  fromDate.setHours(fromTime[0], fromTime[1]);
  toDate.setHours(toTime[0], toTime[1]);
  return (toDate - fromDate)/60000;
}
