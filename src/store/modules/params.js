export default {
  state: {
    tabletView: false,
    isCompany: '',
    balance: '',
    name: '',
    reservations: [],
    userInfo: {
      dataLoaded: false
    },
    companyInfo: {
      dataLoaded: false
    },
    citizenships: [{
        "id": 1,
        "name": "Иран",
        "nameEn": "Iran",
        "code": "IR"
      },
      {
        "id": 2,
        "name": "Гамбия",
        "nameEn": "Gambia",
        "code": "GM"
      },
      {
        "id": 3,
        "name": "Ливан",
        "nameEn": "Lebanon",
        "code": "LB"
      },
      {
        "id": 4,
        "name": "Македония",
        "nameEn": "Macedonia",
        "code": "MK"
      },
      {
        "id": 5,
        "name": "Куракао",
        "nameEn": "Curacao",
        "code": "CW"
      },
      {
        "id": 6,
        "name": "Фиджи",
        "nameEn": "Fiji Islands",
        "code": "FJ"
      },
      {
        "id": 7,
        "name": "Австрия",
        "nameEn": "Austria",
        "code": "AT"
      },
      {
        "id": 8,
        "name": "Сент-Винсент и Гренадины",
        "nameEn": "Saint Vincent And The Grenadines",
        "code": "VC"
      },
      {
        "id": 9,
        "name": "Сингапур",
        "nameEn": "Singapore",
        "code": "SG"
      },
      {
        "id": 10,
        "name": "Швейцария",
        "nameEn": "Switzerland",
        "code": "CH"
      },
      {
        "id": 11,
        "name": "Центральноафриканская Республика",
        "nameEn": "Central African Republic",
        "code": "CF"
      },
      {
        "id": 12,
        "name": "Мавритания",
        "nameEn": "Mauritania",
        "code": "MR"
      },
      {
        "id": 13,
        "name": "Барбадос",
        "nameEn": "Barbados",
        "code": "BB"
      },
      {
        "id": 14,
        "name": "Virgin Islands U.S.",
        "nameEn": "Virgin Islands U.S.",
        "code": "VI"
      },
      {
        "id": 15,
        "name": "Гренада",
        "nameEn": "Grenada",
        "code": "GD"
      },
      {
        "id": 16,
        "name": "Нигер",
        "nameEn": "Niger",
        "code": "NE"
      },
      {
        "id": 17,
        "name": "Мьянма",
        "nameEn": "Myanmar",
        "code": "MM"
      },
      {
        "id": 18,
        "name": "Молдавия",
        "nameEn": "Moldova",
        "code": "MD"
      },
      {
        "id": 19,
        "name": "Аруба",
        "nameEn": "Aruba",
        "code": "AW"
      },
      {
        "id": 20,
        "name": "Египет",
        "nameEn": "Egypt",
        "code": "EG"
      },
      {
        "id": 21,
        "name": "Чили",
        "nameEn": "Chile",
        "code": "CL"
      },
      {
        "id": 22,
        "name": "Колумбия",
        "nameEn": "Colombia",
        "code": "CO"
      },
      {
        "id": 23,
        "name": "Малави",
        "nameEn": "Malawi",
        "code": "MW"
      },
      {
        "id": 24,
        "name": "Виргинские острова",
        "nameEn": "British Virgin Islands",
        "code": "VG"
      },
      {
        "id": 25,
        "name": "Марокко",
        "nameEn": "Morocco",
        "code": "MA"
      },
      {
        "id": 26,
        "name": "Макао",
        "nameEn": "Macau",
        "code": "MO"
      },
      {
        "id": 27,
        "name": "Йемен",
        "nameEn": "Yemen, Republic Of",
        "code": "YE"
      },
      {
        "id": 28,
        "name": "Зимбабве",
        "nameEn": "Zimbabwe",
        "code": "ZW"
      },
      {
        "id": 29,
        "name": "Гибралтар",
        "nameEn": "Gibraltar",
        "code": "GI"
      },
      {
        "id": 30,
        "name": "Доминика",
        "nameEn": "Dominica",
        "code": "DM"
      },
      {
        "id": 31,
        "name": "Мальта",
        "nameEn": "Malta",
        "code": "MT"
      },
      {
        "id": 32,
        "name": "Кокосовые острова",
        "nameEn": "Cocos Islands",
        "code": "CC"
      },
      {
        "id": 33,
        "name": "Bonaire, Saint Eustatius and Saba",
        "nameEn": "Bonaire, Saint Eustatius and Saba",
        "code": "BQ"
      },
      {
        "id": 34,
        "name": "Бахрейн",
        "nameEn": "Bahrain",
        "code": "BH"
      },
      {
        "id": 35,
        "name": "Танзания",
        "nameEn": "Tanzania",
        "code": "TZ"
      },
      {
        "id": 36,
        "name": "Фарерские острова",
        "nameEn": "Faeroe Islands",
        "code": "FO"
      },
      {
        "id": 37,
        "name": "Турция",
        "nameEn": "Turkey",
        "code": "TR"
      },
      {
        "id": 38,
        "name": "Китай",
        "nameEn": "China",
        "code": "CN"
      },
      {
        "id": 39,
        "name": "Тонга",
        "nameEn": "Tonga",
        "code": "TO"
      },
      {
        "id": 40,
        "name": "Тайвань",
        "nameEn": "Taiwan, Republic of China",
        "code": "TW"
      },
      {
        "id": 41,
        "name": "США",
        "nameEn": "United States",
        "code": "US"
      },
      {
        "id": 42,
        "name": "Нигерия",
        "nameEn": "Nigeria",
        "code": "NG"
      },
      {
        "id": 43,
        "name": "Мартиника",
        "nameEn": "Martinique",
        "code": "MQ"
      },
      {
        "id": 44,
        "name": "Таджикистан",
        "nameEn": "Tajikistan",
        "code": "TJ"
      },
      {
        "id": 45,
        "name": "Острова Кука",
        "nameEn": "Cook Islands",
        "code": "CK"
      },
      {
        "id": 46,
        "name": "Того",
        "nameEn": "Togo",
        "code": "TG"
      },
      {
        "id": 47,
        "name": "Камерун",
        "nameEn": "Cameroon, United Republic Of",
        "code": "CM"
      },
      {
        "id": 48,
        "name": "Микронезия",
        "nameEn": "Micronesia",
        "code": "FM"
      },
      {
        "id": 49,
        "name": "Острова Уоллис и Футуна",
        "nameEn": "Wallis and Futuna Islands",
        "code": "WF"
      },
      {
        "id": 50,
        "name": "Бенин",
        "nameEn": "Benin",
        "code": "BJ"
      },
      {
        "id": 51,
        "name": "Перу",
        "nameEn": "Peru",
        "code": "PE"
      },
      {
        "id": 52,
        "name": "Sint Maarten",
        "nameEn": "Sint Maarten",
        "code": "SX"
      },
      {
        "id": 53,
        "name": "Свазиленд",
        "nameEn": "Swaziland",
        "code": "SZ"
      },
      {
        "id": 54,
        "name": "Соломоновы острова",
        "nameEn": "Solomon Islands",
        "code": "SB"
      },
      {
        "id": 55,
        "name": "Гваделупа",
        "nameEn": "Guadeloupe",
        "code": "GP"
      },
      {
        "id": 56,
        "name": "Мозамбик",
        "nameEn": "Mozambique",
        "code": "MZ"
      },
      {
        "id": 57,
        "name": "Санкт Мартин",
        "nameEn": "Saint Martin",
        "code": "MF"
      },
      {
        "id": 58,
        "name": "Дания",
        "nameEn": "Denmark",
        "code": "DK"
      },
      {
        "id": 59,
        "name": "Остров Рождества",
        "nameEn": "Christmas Island",
        "code": "CX"
      },
      {
        "id": 60,
        "name": "Алжир",
        "nameEn": "Algeria",
        "code": "DZ"
      },
      {
        "id": 61,
        "name": "Аргентина",
        "nameEn": "Argen",
        "code": "AR"
      },
      {
        "id": 62,
        "name": "Остров Норфолк",
        "nameEn": "Norfolk Island",
        "code": "NF"
      },
      {
        "id": 63,
        "name": "Конго, Демократическая Республика",
        "nameEn": "Congo Democratic Republic Of",
        "code": "CD"
      },
      {
        "id": 64,
        "name": "Сенегал",
        "nameEn": "Senegal",
        "code": "SN"
      },
      {
        "id": 65,
        "name": "Израиль",
        "nameEn": "Israel",
        "code": "IL"
      },
      {
        "id": 66,
        "name": "Экваториальная Гвинея",
        "nameEn": "Equatorial Guinea",
        "code": "GQ"
      },
      {
        "id": 67,
        "name": "Гватемала",
        "nameEn": "Guatemala",
        "code": "GT"
      },
      {
        "id": 68,
        "name": "Новая Зеландия",
        "nameEn": "New Zealand",
        "code": "NZ"
      },
      {
        "id": 69,
        "name": "ЮАР",
        "nameEn": "South Africa Republic",
        "code": "ZA"
      },
      {
        "id": 70,
        "name": "Малайзия",
        "nameEn": "Malaysia",
        "code": "MY"
      },
      {
        "id": 71,
        "name": "Монголия",
        "nameEn": "Mongolia",
        "code": "MN"
      },
      {
        "id": 72,
        "name": "Вануату",
        "nameEn": "Vanuatu",
        "code": "VU"
      },
      {
        "id": 73,
        "name": "Ирак",
        "nameEn": "Iraq",
        "code": "IQ"
      },
      {
        "id": 74,
        "name": "Финляндия",
        "nameEn": "Finland",
        "code": "FI"
      },
      {
        "id": 75,
        "name": "Сент-Люсия",
        "nameEn": "Saint Lucia",
        "code": "LC"
      },
      {
        "id": 76,
        "name": "Исландия",
        "nameEn": "Iceland",
        "code": "IS"
      },
      {
        "id": 77,
        "name": "Сьерра-Леоне",
        "nameEn": "Sierra Leone",
        "code": "SL"
      },
      {
        "id": 78,
        "name": "Корея (Северная), КНДР",
        "nameEn": "Korea, Democratic Peoples Republic",
        "code": "KP"
      },
      {
        "id": 79,
        "name": "Эстония",
        "nameEn": "Estonia",
        "code": "EE"
      },
      {
        "id": 80,
        "name": "Ангола",
        "nameEn": "Angola",
        "code": "AO"
      },
      {
        "id": 81,
        "name": "Бельгия",
        "nameEn": "Belgium",
        "code": "BE"
      },
      {
        "id": 82,
        "name": "Науру",
        "nameEn": "Nauru",
        "code": "NR"
      },
      {
        "id": 83,
        "name": "Ливия",
        "nameEn": "Libyan Arab Jamahiriya",
        "code": "LY"
      },
      {
        "id": 84,
        "name": "Таиланд",
        "nameEn": "Thailand",
        "code": "TH"
      },
      {
        "id": 85,
        "name": "Эль-Сальвадор",
        "nameEn": "El Salvador",
        "code": "SV"
      },
      {
        "id": 86,
        "name": "Доминиканская Республика",
        "nameEn": "Dominican Republic",
        "code": "DO"
      },
      {
        "id": 87,
        "name": "Белиз",
        "nameEn": "Belize",
        "code": "BZ"
      },
      {
        "id": 88,
        "name": "Монако",
        "nameEn": "Monaco",
        "code": "MC"
      },
      {
        "id": 89,
        "name": "Палестина",
        "nameEn": "Occupied Palestinian Territory",
        "code": "PS"
      },
      {
        "id": 90,
        "name": "Мальдивы",
        "nameEn": "Maldives",
        "code": "MV"
      },
      {
        "id": 91,
        "name": "Антигуа и Барбуда",
        "nameEn": "Antigua And Barbuda",
        "code": "AG"
      },
      {
        "id": 92,
        "name": "Гана",
        "nameEn": "GHANA",
        "code": "GH"
      },
      {
        "id": 93,
        "name": "Индонезия",
        "nameEn": "Indonesia",
        "code": "ID"
      },
      {
        "id": 94,
        "name": "Катар",
        "nameEn": "Qatar",
        "code": "QA"
      },
      {
        "id": 95,
        "name": "Папуа-Новая Гвинея",
        "nameEn": "Papua New Guinea (Niugini)",
        "code": "PG"
      },
      {
        "id": 96,
        "name": "Эфиопия",
        "nameEn": "Ethiopia",
        "code": "ET"
      },
      {
        "id": 97,
        "name": "Республика Крым",
        "nameEn": "Republic of Crimea",
        "code": "AK"
      },
      {
        "id": 98,
        "name": "Гаити",
        "nameEn": "Haiti",
        "code": "HT"
      },
      {
        "id": 99,
        "name": "Чехия",
        "nameEn": "Czech Republic",
        "code": "CZ"
      },
      {
        "id": 100,
        "name": "Восточный Тимор",
        "nameEn": "Timor Leste",
        "code": "TL"
      },
      {
        "id": 101,
        "name": "Румыния",
        "nameEn": "Romania",
        "code": "RO"
      },
      {
        "id": 102,
        "name": "Эквадор",
        "nameEn": "Ecuador",
        "code": "EC"
      },
      {
        "id": 103,
        "name": "Руанда",
        "nameEn": "Rwanda",
        "code": "RW"
      },
      {
        "id": 104,
        "name": "Гайана",
        "nameEn": "Guyana",
        "code": "GY"
      },
      {
        "id": 105,
        "name": "Судан",
        "nameEn": "Sudan",
        "code": "SD"
      },
      {
        "id": 106,
        "name": "Армения",
        "nameEn": "Armenia",
        "code": "AM"
      },
      {
        "id": 107,
        "name": "Полинезия",
        "nameEn": "French Polynesia",
        "code": "PF"
      },
      {
        "id": 108,
        "name": "Сомали",
        "nameEn": "Somalia",
        "code": "SO"
      },
      {
        "id": 109,
        "name": "Сан-Томе и Принсипи",
        "nameEn": "Sao Tome and Principe",
        "code": "ST"
      },
      {
        "id": 110,
        "name": "Реюньон",
        "nameEn": "Reunion",
        "code": "RE"
      },
      {
        "id": 111,
        "name": "Италия",
        "nameEn": "Italy",
        "code": "IT"
      },
      {
        "id": 112,
        "name": "Палау",
        "nameEn": "Palau",
        "code": "PW"
      },
      {
        "id": 113,
        "name": "Новая Каледония",
        "nameEn": "New Caledonia",
        "code": "NC"
      },
      {
        "id": 114,
        "name": "Джибути",
        "nameEn": "Djibouti",
        "code": "DJ"
      },
      {
        "id": 115,
        "name": "Сент-Китс и Невис",
        "nameEn": "St. Kitts - Nevis",
        "code": "KN"
      },
      {
        "id": 116,
        "name": "Бразилия",
        "nameEn": "Brazil",
        "code": "BR"
      },
      {
        "id": 117,
        "name": "Швеция",
        "nameEn": "Sweden",
        "code": "SE"
      },
      {
        "id": 118,
        "name": "Каймановы острова",
        "nameEn": "Cayman Islands",
        "code": "KY"
      },
      {
        "id": 119,
        "name": "Монтсеррат",
        "nameEn": "Montserrat",
        "code": "MS"
      },
      {
        "id": 120,
        "name": "Парагвай",
        "nameEn": "Paraguay",
        "code": "PY"
      },
      {
        "id": 121,
        "name": "Латвия",
        "nameEn": "Latvia",
        "code": "LV"
      },
      {
        "id": 122,
        "name": "Бермуды",
        "nameEn": "Bermuda",
        "code": "BM"
      },
      {
        "id": 123,
        "name": "Хорватия",
        "nameEn": "Croatia",
        "code": "HR"
      },
      {
        "id": 124,
        "name": "Кирибати",
        "nameEn": "Kiribati",
        "code": "KI"
      },
      {
        "id": 125,
        "name": "Ботсвана",
        "nameEn": "Botswana",
        "code": "BW"
      },
      {
        "id": 126,
        "name": "Гвиана",
        "nameEn": "French Guiana",
        "code": "GF"
      },
      {
        "id": 127,
        "name": "Суринам",
        "nameEn": "Suriname",
        "code": "SR"
      },
      {
        "id": 128,
        "name": "Буркина-Фасо",
        "nameEn": "Burkina Faso",
        "code": "BF"
      },
      {
        "id": 129,
        "name": "Гренландия",
        "nameEn": "Greenland",
        "code": "GL"
      },
      {
        "id": 130,
        "name": "Бурунди",
        "nameEn": "Burundi",
        "code": "BI"
      },
      {
        "id": 131,
        "name": "Тринидад и Тобаго",
        "nameEn": "Trinidad and Tobago",
        "code": "TT"
      },
      {
        "id": 132,
        "name": "Филиппины",
        "nameEn": "Philippines",
        "code": "PH"
      },
      {
        "id": 133,
        "name": "Самоа",
        "nameEn": "Samoa, Independent State Of",
        "code": "WS"
      },
      {
        "id": 134,
        "name": "Украина",
        "nameEn": "Ukraine",
        "code": "UA"
      },
      {
        "id": 135,
        "name": "Уругвай",
        "nameEn": "Uruguay",
        "code": "UY"
      },
      {
        "id": 136,
        "name": "Бангладеш",
        "nameEn": "Bangladesh",
        "code": "BD"
      },
      {
        "id": 137,
        "name": "Марианские острова",
        "nameEn": "Marianna Islands",
        "code": "MP"
      },
      {
        "id": 138,
        "name": "Туркменистан",
        "nameEn": "Turkmenistan",
        "code": "TM"
      },
      {
        "id": 139,
        "name": "Сербия",
        "nameEn": "Serbia",
        "code": "RS"
      },
      {
        "id": 140,
        "name": "Либерия",
        "nameEn": "Liberia",
        "code": "LR"
      },
      {
        "id": 141,
        "name": "Гондурас",
        "nameEn": "Honduras",
        "code": "HN"
      },
      {
        "id": 142,
        "name": "Лаос",
        "nameEn": "Lao, People's Dem. Rep.",
        "code": "LA"
      },
      {
        "id": 143,
        "name": "Камбоджа",
        "nameEn": "Cambodia",
        "code": "KH"
      },
      {
        "id": 144,
        "name": "Гвинея-Биссау",
        "nameEn": "Guinea Bissau",
        "code": "GW"
      },
      {
        "id": 145,
        "name": "Сент-Пьер и Микелон",
        "nameEn": "St. Pierre and Miquelon",
        "code": "PM"
      },
      {
        "id": 146,
        "name": "Шри-Ланка",
        "nameEn": "Sri Lanka",
        "code": "LK"
      },
      {
        "id": 147,
        "name": "Американское Самоа",
        "nameEn": "Samoa, American",
        "code": "AS"
      },
      {
        "id": 148,
        "name": "Маршалловы острова",
        "nameEn": "Marshall Islands",
        "code": "MH"
      },
      {
        "id": 149,
        "name": "Гуам",
        "nameEn": "Guam",
        "code": "GU"
      },
      {
        "id": 150,
        "name": "Бутан",
        "nameEn": "Bhutan",
        "code": "BT"
      },
      {
        "id": 151,
        "name": "Кабо-Верде",
        "nameEn": "Cape Verde, Republic Of",
        "code": "CV"
      },
      {
        "id": 152,
        "name": "Саудовская Аравия",
        "nameEn": "Saudi Arabia",
        "code": "SA"
      },
      {
        "id": 153,
        "name": "Пуэрто-Рико",
        "nameEn": "Puerto Rico",
        "code": "PR"
      },
      {
        "id": 154,
        "name": "Мексика",
        "nameEn": "Mexico",
        "code": "MX"
      },
      {
        "id": 155,
        "name": "Тунис",
        "nameEn": "Tunisia",
        "code": "TN"
      },
      {
        "id": 156,
        "name": "Венгрия",
        "nameEn": "Hungary",
        "code": "HU"
      },
      {
        "id": 157,
        "name": "Saint Barthelemy",
        "nameEn": "Saint Barthelemy",
        "code": "BL"
      },
      {
        "id": 158,
        "name": "Боливия",
        "nameEn": "Bolivia",
        "code": "BO"
      },
      {
        "id": 159,
        "name": "ОАЭ",
        "nameEn": "United Arab Emirates",
        "code": "AE"
      },
      {
        "id": 160,
        "name": "Вьетнам",
        "nameEn": "Vietnam",
        "code": "VN"
      },
      {
        "id": 161,
        "name": "Австралия",
        "nameEn": "Australia",
        "code": "AU"
      },
      {
        "id": 162,
        "name": "Тувалу",
        "nameEn": "Tuvalu",
        "code": "TV"
      },
      {
        "id": 163,
        "name": "Непал",
        "nameEn": "Nepal",
        "code": "NP"
      },
      {
        "id": 164,
        "name": "Кувейт",
        "nameEn": "Kuwait",
        "code": "KW"
      },
      {
        "id": 165,
        "name": "Лесото",
        "nameEn": "Lesotho",
        "code": "LS"
      },
      {
        "id": 166,
        "name": "Словения",
        "nameEn": "Slovenia",
        "code": "SI"
      },
      {
        "id": 167,
        "name": "Ангилья",
        "nameEn": "Anguilla",
        "code": "AI"
      },
      {
        "id": 168,
        "name": "Венесуэла",
        "nameEn": "Venezuela",
        "code": "VE"
      },
      {
        "id": 169,
        "name": "Черногория",
        "nameEn": "Montenegro",
        "code": "ME"
      },
      {
        "id": 170,
        "name": "Маврикий",
        "nameEn": "Mauritius",
        "code": "MU"
      },
      {
        "id": 171,
        "name": "Болгария",
        "nameEn": "Bulgaria",
        "code": "BG"
      },
      {
        "id": 172,
        "name": "Острова Теркс и Кайкос",
        "nameEn": "Turks And Caicos Islands",
        "code": "TC"
      },
      {
        "id": 173,
        "name": "Индия",
        "nameEn": "India",
        "code": "IN"
      },
      {
        "id": 174,
        "name": "Оман",
        "nameEn": "Oman, Sultanate Of",
        "code": "OM"
      },
      {
        "id": 175,
        "name": "Замбия",
        "nameEn": "Zambia",
        "code": "ZM"
      },
      {
        "id": 176,
        "name": "Норвегия",
        "nameEn": "Norway",
        "code": "NO"
      },
      {
        "id": 177,
        "name": "Фолклендские острова",
        "nameEn": "Falkland Islands",
        "code": "FK"
      },
      {
        "id": 178,
        "name": "Киргизия",
        "nameEn": "Kyrgyzstan",
        "code": "KG"
      },
      {
        "id": 179,
        "name": "Кипр",
        "nameEn": "Cyprus",
        "code": "CY"
      },
      {
        "id": 180,
        "name": "Конго Республика",
        "nameEn": "Congo Republic",
        "code": "CG"
      },
      {
        "id": 181,
        "name": "Азербайджан",
        "nameEn": "Azerbaijan",
        "code": "AZ"
      },
      {
        "id": 182,
        "name": "Габон",
        "nameEn": "Gabon",
        "code": "GA"
      },
      {
        "id": 183,
        "name": "Узбекистан",
        "nameEn": "Uzbekistan",
        "code": "UZ"
      },
      {
        "id": 184,
        "name": "Люксембург",
        "nameEn": "Luxembourg",
        "code": "LU"
      },
      {
        "id": 185,
        "name": "Коморские острова",
        "nameEn": "Comoros",
        "code": "KM"
      },
      {
        "id": 186,
        "name": "Уганда",
        "nameEn": "Uganda",
        "code": "UG"
      },
      {
        "id": 187,
        "name": "Германия",
        "nameEn": "Germany",
        "code": "DE"
      },
      {
        "id": 188,
        "name": "Великобритания",
        "nameEn": "United Kingdom",
        "code": "GB"
      },
      {
        "id": 189,
        "name": "Абхазия",
        "nameEn": "Abkhazia",
        "code": "AB"
      },
      {
        "id": 190,
        "name": "Казахстан",
        "nameEn": "Kazakhstan",
        "code": "KZ"
      },
      {
        "id": 191,
        "name": "Мали",
        "nameEn": "Mali",
        "code": "ML"
      },
      {
        "id": 192,
        "name": "Эритрея",
        "nameEn": "Eritrea",
        "code": "ER"
      },
      {
        "id": 193,
        "name": "Багамы",
        "nameEn": "Bahamas",
        "code": "BS"
      },
      {
        "id": 194,
        "name": "Кения",
        "nameEn": "Kenya",
        "code": "KE"
      },
      {
        "id": 195,
        "name": "Майотт",
        "nameEn": "Mayotte",
        "code": "YT"
      },
      {
        "id": 196,
        "name": "Ирландия",
        "nameEn": "Ireland, Republic Of",
        "code": "IE"
      },
      {
        "id": 197,
        "name": "Сейшельские острова",
        "nameEn": "Seychelles Islands",
        "code": "SC"
      },
      {
        "id": 198,
        "name": "Португалия",
        "nameEn": "Portugal",
        "code": "PT"
      },
      {
        "id": 199,
        "name": "Остров Святой Елены",
        "nameEn": "Ascension Island/St. Helena",
        "code": "SH"
      },
      {
        "id": 200,
        "name": "Панама",
        "nameEn": "Panama",
        "code": "PA"
      },
      {
        "id": 201,
        "name": "Ниуэ",
        "nameEn": "Niue",
        "code": "NU"
      },
      {
        "id": 202,
        "name": "Япония",
        "nameEn": "Japan",
        "code": "JP"
      },
      {
        "id": 203,
        "name": "Албания",
        "nameEn": "Albania",
        "code": "AL"
      },
      {
        "id": 204,
        "name": "Корея (Южная)",
        "nameEn": "Korea, Republic Of",
        "code": "KR"
      },
      {
        "id": 205,
        "name": "Россия",
        "nameEn": "Russian Federation",
        "code": "RU"
      },
      {
        "id": 206,
        "name": "Мадагаскар",
        "nameEn": "Madagascar (Malagasy)",
        "code": "MG"
      },
      {
        "id": 207,
        "name": "Гонконг",
        "nameEn": "Hong Kong",
        "code": "HK"
      },
      {
        "id": 208,
        "name": "Франция",
        "nameEn": "France",
        "code": "FR"
      },
      {
        "id": 209,
        "name": "Кот-д'Ивуар",
        "nameEn": "Cote d'Ivoire",
        "code": "CI"
      },
      {
        "id": 210,
        "name": "Коста-Рика",
        "nameEn": "Costa Rica",
        "code": "CR"
      },
      {
        "id": 211,
        "name": "Словакия",
        "nameEn": "Slovakia",
        "code": "SK"
      },
      {
        "id": 212,
        "name": "Афганистан",
        "nameEn": "Afghanistan",
        "code": "AF"
      },
      {
        "id": 213,
        "name": "Куба",
        "nameEn": "Cuba",
        "code": "CU"
      },
      {
        "id": 214,
        "name": "Испания",
        "nameEn": "Spain",
        "code": "ES"
      },
      {
        "id": 215,
        "name": "Никарагуа",
        "nameEn": "Nicaragua",
        "code": "NI"
      },
      {
        "id": 216,
        "name": "Грузия",
        "nameEn": "Georgia",
        "code": "GE"
      },
      {
        "id": 217,
        "name": "Ямайка",
        "nameEn": "Jamaica",
        "code": "JM"
      },
      {
        "id": 218,
        "name": "Сирия",
        "nameEn": "Syrian Arab Rep.",
        "code": "SY"
      },
      {
        "id": 219,
        "name": "Польша",
        "nameEn": "Poland",
        "code": "PL"
      },
      {
        "id": 220,
        "name": "Нидерланды",
        "nameEn": "Netherlands",
        "code": "NL"
      },
      {
        "id": 221,
        "name": "Чад",
        "nameEn": "Chad",
        "code": "TD"
      },
      {
        "id": 222,
        "name": "Греция",
        "nameEn": "Greece",
        "code": "GR"
      },
      {
        "id": 223,
        "name": "Намибия",
        "nameEn": "Namibia",
        "code": "NA"
      },
      {
        "id": 224,
        "name": "Беларусь",
        "nameEn": "Belarus",
        "code": "BY"
      },
      {
        "id": 225,
        "name": "Иордания",
        "nameEn": "Jordan",
        "code": "JO"
      },
      {
        "id": 226,
        "name": "Канада",
        "nameEn": "Canada",
        "code": "CA"
      },
      {
        "id": 227,
        "name": "Пакистан",
        "nameEn": "Pakistan",
        "code": "PK"
      },
      {
        "id": 228,
        "name": "Литва",
        "nameEn": "Lithuania",
        "code": "LT"
      },
      {
        "id": 229,
        "name": "Бруней",
        "nameEn": "Brunei",
        "code": "BN"
      },
      {
        "id": 230,
        "name": "Босния и Герцеговина",
        "nameEn": "Bosnia Herzegovina",
        "code": "BA"
      },
      {
        "id": 231,
        "name": "Гвинея",
        "nameEn": "Guinea",
        "code": "GN"
      }
    ],
    classes: [{
    	// Economy
      id: 0,
      count: 0
    }, {
    	// Business
      id: 1,
      count: 0
    }],
    travelerTypes: [{
      id: 1,
      name: 'adults',
      title: {
        ru: 'Взрослый',
        en: 'Adult'
      },
      count: 0,
      selected: false
    }, {
      id: 2,
      name: 'children',
      title: {
        ru: 'Дети(до 18 лет)',
        en: 'Children(under 18 years old)'
      },
      count: 0,
      selected: false
    }],
    passagerTypes: [{
      id: 1,
      name: 'adults',
      title: 'Взрослый',
      count: 0,
      selected: false
    }, {
      id: 2,
      name: 'children',
      title: 'Дети от 2 до 12 лет',
      count: 0,
      selected: false
    }, {
      id: 3,
      name: 'infants',
      title: 'Дети до 2 лет',
      count: 0,
      selected: false
    }]
  },
  mutations: {
    changeTabletView(state, payload) {
      state.tabletView = payload;
    },
    setInitData(state, payload) {
      state.isCompany = payload.isCompany;
      state.name = payload.name;
      state.balance = payload.balance;
      if (payload.isCompany) state.companyId = payload.companyId
    },
    setNewBalance(state, payload) {
      state.balance = payload;
    },
    setReservationForMainPage(state, payload) {
      state.reservations = payload;
    },
    updateUserInfo(state, paylod) {
      Object.assign(state.userInfo, paylod)
    },
    updateCompanyInfo(state, paylod) {
      Object.assign(state.companyInfo, paylod)
    }
  },
  actions: {

  },
  getters: {
    getUserFullInfo: (state, payload) => () => {
      return state.userInfo
    },
    getCompanyFullInfo: (state, payload) => () => {
      return state.companyInfo
    },
    getCitizenships: (state, payload) => () => {
      return state.citizenships;
    },
    getUserName: (state, payload) => () => {
      return state.name
    },
    getUserBalance: (state, payload) => () => {
      return state.balance
    },
    isCompany: (state, payload) => () => {
      return state.isCompany
    }
  }
}
