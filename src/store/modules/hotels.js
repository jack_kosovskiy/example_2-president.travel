import Vue from 'vue';
export default {
  state: {
    hotels: [],
    searchInfo: {
      from: "",
      to: "",
      city: "",
      country: "",
      travelers: "",
      rooms: "",
      selectedPassengers: [],
    },
    history_id: "",
    filters: {},
    selectedHotel: "",
    selectedRoom: {},
    contractInfo: {},
    confirmInfo: {},
    searchRequest: {}
  },
  mutations: {
    setSearchRequest(state, payload) {
      Vue.set(state, 'searchRequest', payload)
    },
    addHotelPhotos(state, payload) {
      for (let index = 0; index < state.hotels.length; index++) {
        const element = state.hotels[index];
        if (element.id == state.selectedHotel) {
          Vue.set(element, 'photos', payload)
        }
      }
    },
    setConfirmInfo(state, payload) {
      state.confirmInfo = payload
    },
    addContractInfo(state, payload) {
      state.contractInfo = payload
    },
    loadRooms(state, payload) {
    	let currentHotel = state.hotels.find(element => element.id === state.selectedHotel);
		Vue.set(currentHotel, 'rooms_info', payload)
    },
	  loadHotelInfo(state, payload) {
		  let currentHotel = state.hotels.find(element => element.id === state.selectedHotel);
		  Vue.set(currentHotel, 'hotel_info', payload)
	  },
    setFilters(state, payload) {
      state.filters = payload
    },
    updateFilters(state, payload) {
      Object.assign({}, state.filters, payload)
    },
    setHotel(state, payload) {
      state.hotels = payload;
    },
    setHistoryId(state, payload) {
      state.history_id = payload
    },
    setSearchInfo(state, payload) {
      state.searchInfo.from = payload.from;
      state.searchInfo.to = payload.to;
      state.searchInfo.city = payload.city;
      state.searchInfo.country = payload.country;
      state.searchInfo.travelers = payload.travelers;
      state.searchInfo.rooms = payload.rooms;
      state.searchInfo.selectedPassengers = payload.selectedPassengers;
    },
    setSelectedHotel(state, payload) {
      state.selectedHotel = payload;
    },
    setSelectedRoom(state, payload) {
      state.selectedRoom = payload;
    }
  },
  actions: {},
  getters: {
    getSelectedHotel: state => state.hotels.find(element => element.id === state.selectedHotel)
  }
}
