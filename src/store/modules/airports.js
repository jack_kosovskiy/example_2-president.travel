export default {
  state:{
    airlines:{},
    alliances:{},
    airports:{}
    
  },
  mutations:{
    loadAlliances(state, payload){
        state.alliances = payload
    },
    loadAirlines(state, payload){
      state.airlines = payload;
    },
    loadAirports(state, payload){
      state.airports = payload;
    }

  },
  actions: {

  },
  getters:{
    getAirportNameByCode: (state, getters) => (code) => {
        for (let index = 0; index < state.airports.airportsForwardFrom.length; index++) {
            const element = state.airports.airportsForwardFrom[index];
            if(element.code == code){
                return element.name;
                break;
            }
        }
    },
    getAirportById: (state, getters) => (code) =>{
      return state.airports[code]
    },
    getAirlineById: (state, getters) => (code) =>{
      return state.airlines[code]
    },
    //первые 2 геттера надо удалить после теста всего приложения

    
    getAirportByCode: (state, getters) => (code) =>{
      return state.airports[code]
    },
    getAirlineByCode: (state, getters) => (code) =>{
      return state.airlines[code]
    },
    getCityByCode: (state, getters) => (code) =>{
      return state.cities[code]
    }
    // filteredFlights: state => {
    //   let flightsList = state.flightsList;
    //   let index = flightsList.length;
    //   while(index--){
    //     let flight = flightsList[index];
    //     flight.filghtsTo = flight.filghtsTo.filter(item=>{
    //       if(state.selectedFlights.indexOf(item.id) !== -1){
    //         return item;
    //       }
    //     })
    //   }
    // }

  }
}
