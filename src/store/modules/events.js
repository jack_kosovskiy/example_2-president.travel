export default {
  state: {
    events: {}
  },
  mutations: {
    setEvents(state, payload) {
      state.events = payload;
    }
  },
  getters: {
    getEvents: (state, getters) => () => {
      return state.events
    },
    getEventsByDate: (state, getters) => (date) => {
      return state.events[date]
    }
  }
};
