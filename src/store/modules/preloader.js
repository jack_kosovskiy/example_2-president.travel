export default {
  state:{
    isHotel: false,
    isPreloaderShowed: false
  },
  mutations:{
    setIsHotel(state, payload){
      state.isHotel = true;
    },
    setIsTicket(state, payload){
      state.isHotel = false;
    },
    runPreloader(state, payload){
      state.isPreloaderShowed = true;
    },
    stopPreloader(state, payload){
      state.isPreloaderShowed = false;
    }

  },
  actions: {

  },
  getters:{

  }
}
