export default {
  state: {
    langs: [{
        "id": 1,
        "title": "Русский",
        "symbol": "&#x584;",
        "code": "ru"
      },
      {
        "id": 2,
        "title": "English",
        "symbol": "&#36;",
        "code": "en"
      }
    ],
    currentLang: {
      "id": 2,
      "title": "English",
      "symbol": "&#36;",
      "code": "en"
    }

  },
  mutations: {
    changeLang(state, payload) {
      state.currentLang = payload;
    }

  },
  actions: {

  },
  getters: {

  }
}
