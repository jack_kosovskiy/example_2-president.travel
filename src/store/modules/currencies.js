export default {
  state: {
    currencies: [{
        "id": 1,
        "title": "Рубль",
        "symbol": "&#8381;",
        "code": "rub"
      },
      {
        "id": 2,
        "title": "Dollar",
        "symbol": "&#36;",
        "code": "usd"
      },
      {
        "id": 3,
        "title": "Euro",
        "symbol": "&euro;",
        "code": "eur"
      },
      {
        "id": 4,
        "title": "Pound",
        "symbol": "&pound;",
        "code": "gbp"
      },
      {
        "id": 5,
        "title": "元",
        "symbol": "&#65509;",
        "code": "jpy"
      }
    ],
    selectedCurrency: {
      "id": 2,
      "title": "Dollar",
      "symbol": "&#36;",
      "code": "usd"
    }
  },
  mutations: {
    selectCurrency(state, payload) {
      state.selectedCurrency = payload;
    }

  },
  actions: {

  },
  getters: {

  }
}
