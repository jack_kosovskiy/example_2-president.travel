export default {
	state: {
		isRoundTripSearch: false,
		days: 0,
		guests: {
			adults: 0,
			childs: 0,
			infants: 0,
			rooms: 0
		},
		from: {
			city: "",
			country: "",
			abbr: "",
			date: ""
		},
		to: {
			city: "",
			country: "",
			abbr: "",
			date: ""
		},
		selectedPassengers: [],
		newPassengers: {
			adults: 1,
			children: 0,
			infants: 0,
			class: 0
		},
		hotelParams: {
			newPassengers: {
				adults: 1,
				children: 0,
			},
			fromDate: "",
			toDate: "",
			room: 1,
			childrenAge: [],
			city: {
				abbr: "",
				city: "",
				country: "",
			}
		}
	},
	mutations: {
		changeRoom(state, payload) {
			state.hotelParams.room = payload;
		},
		changeChildrenAge(state, payload) {
			state.hotelParams.childrenAge = payload;
		},
		addChildren(state, payload) {
			state.hotelParams.childrenAge.push(0)
		},
		removeChildren(state, payload) {
			state.hotelParams.childrenAge.splice(-1, 1);
		},
		changeClass(state, payload) {
			for (let index = 0; index < state.selectedPassengers.length; index++) {
				if (state.selectedPassengers[index].travelerId == payload.travelerId) {
					state.selectedPassengers[index].classID = payload.class;
					break;
				}
			}
		},
		changeRoundTripSearch(state, payload) {
			state.isRoundTripSearch = payload;
		},
		changeFrom2To(state, payload) {
			var temp1 = Object.assign({}, state.from);
			var temp2 = Object.assign({}, state.to);
			state.from.city = temp2.city;
			state.from.country = temp2.country;
			state.from.abbr = temp2.abbr;
			state.to.city = temp1.city;
			state.to.country = temp1.country;
			state.to.abbr = temp1.abbr;
		},
		increaseNewPassengersCount(state, payload) {
			state.newPassengers[payload] += 1;
		},
		descreaseNewPassengersCount(state, payload) {
			if (state.newPassengers[payload] !== 0) {
				state.newPassengers[payload] -= 1;
			}
		},
		increaseNewHotelPassengersCount(state, payload) {
			state.hotelParams.newPassengers[payload] += 1;
		},
		descreaseNewHotelPassengersCount(state, payload) {
			if (state.hotelParams.newPassengers[payload] !== 0) {
				state.hotelParams.newPassengers[payload] -= 1;
			}
		},
		updateSearchInfoRooms(state, payload) {
			state.hotelParams.room = payload;
		},
		changeNewPassengersClass(state, payload) {
			state.newPassengers.class = payload;
		},
		increaseTotalCount(state) {
			state.totalCount += 1;
		},
		decreaseTotalCount(state) {
			if (state.totalCount !== 0) {
				state.totalCount -= 1;
			}
		},
		setFromDate(state, payload) {
			state.from.date = payload;
		},
		setToDate(state, payload) {
			state.to.date = payload;
		},
		setHotelFromDate(state, payload) {
			state.hotelParams.fromDate = payload;
		},
		setHotelToDate(state, payload) {
			state.hotelParams.toDate = payload;
		},
		setDays(state, payload) {
			state.days = payload;
		},
		setFromCity(state, payload) {
			// state.from.id = payload.id;
			state.from.city = payload.city;
			state.from.country = payload.country;
			state.from.abbr = payload.abbr;
		},
		setToCity(state, payload) {
			// state.to.id = payload.id;
			state.to.city = payload.city;
			state.to.country = payload.country;
			state.to.abbr = payload.abbr;
		},
		setHotelCity(state, payload) {
			state.hotelParams.city.city = payload.city;
			state.hotelParams.city.country = payload.country;
			state.hotelParams.city.abbr = payload.abbr;
		},
		addSelectedPassengers(state, payload) {
			state.selectedPassengers.push(payload);
		},
		removeSelectedPassengers(state, payload) {
			state.selectedPassengers.forEach(function (element, index, array) {
				if (element.travelerId == payload.travelerId) {
					array.splice(index, 1);
				}
			});
		}
	},
	actions: {},
	getters: {
		getIsRoundTripSearch: (state, getters) => () => {
			return state.isRoundTripSearch;
		},
		getNewPassengersCount: (state, getters) => type => {
			return state.newPassengers[type];
		},
		getNewHotelPassengersCount: (state, getters) => type => {
			return state.hotelParams.newPassengers[type];
		},
		getSelectedPassId: (state, getters) => () => {
			var array = [];
			state.selectedPassengers.forEach(function (el, index, arr) {
				array.push(el.id);
			});
			return array;
		}
	}
};
