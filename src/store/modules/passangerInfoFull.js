export default {
  state: {
    passengerinfofull: [
      {
        id: 2,
        travelerId: 2,
        firstname: "Anna",
        lastname: "Vaechkin",
        fathersname: "Victorovna",
        birthDate: "25-12-2000",
        sex: 0,
        sexName: "Женский",
        phone: "123123123",
        email: "User@gmail.com",
        citizenship: [
          {
            citizenship_country_id: 7,
            country_name: "Австрия"
          }
        ],
        document: [
          {
            type: 1,
            typeName: "Загран паспорт",
            number: "123123"
          },
          {
            type: 0,
            typeName: "Внутр. паспорт",
            number: "33333"
          }
        ],
        preferenceCards: [
          {
            name: "Аэрофлот",
            number: "123123123"
          }
        ]
      },
      {
        id: 1,
        travelerId: 1,
        firstname: "Vasyl",
        lastname: "Vaechkin",
        fathersname: "Victorovna",
        birthDate: "25-12-2000",
        sex: 0,
        sexName: "Женский",
        phone: "123123123",
        email: "User@gmail.com",
        citizenship: [
          {
            citizenship_country_id: 7,
            country_name: "Австрия"
          }
        ],
        document: [
          {
            type: 1,
            typeName: "Загран паспорт",
            number: "123123"
          },
          {
            type: 0,
            typeName: "Внутр. паспорт",
            number: "33333"
          }
        ],
        preferenceCards: [
          {
            name: "Аэрофлот",
            number: "123123123"
          }
        ]
      },
      {
        id: 3,
        travelerId: 3,
        firstname: "Zipa",
        lastname: "Lipa",
        fathersname: "Victorovna",
        birthDate: "25-12-2000",
        sex: 0,
        sexName: "Женский",
        phone: "123123123",
        email: "User@gmail.com",
        citizenship: [
          {
            citizenship_country_id: 7,
            country_name: "Австрия"
          }
        ],
        document: [
          {
            type: 1,
            typeName: "Загран паспорт",
            number: "123123"
          },
          {
            type: 0,
            typeName: "Внутр. паспорт",
            number: "33333"
          }
        ],
        preferenceCards: [
          {
            name: "Аэрофлот",
            number: "123123123"
          }
        ]
      },
      {
        id: 4,
        travelerId: 4,
        firstname: "Zupa",
        lastname: "Lupa",
        fathersname: "Victorovna",
        birthDate: "25-12-2000",
        sex: 0,
        sexName: "Женский",
        phone: "123123123",
        email: "User@gmail.com",
        citizenship: [
          {
            citizenship_country_id: 7,
            country_name: "Австрия"
          }
        ],
        document: [
          {
            type: 1,
            typeName: "Загран паспорт",
            number: "123123"
          },
          {
            type: 0,
            typeName: "Внутр. паспорт",
            number: "33333"
          }
        ],
        preferenceCards: [
          {
            name: "Аэрофлот",
            number: "123123123"
          }
        ]
      }
    ]
  },
  mutations: {
    addFullInfoPassanger(state, payload) {
      state.passengerinfofull.push(payload);
    }
  },
  getters: {
    getFullPassangerById: (state, getters) => id => {
      for (
        var j = 0, length = state.passengerinfofull.length;
        j < length;
        j++
      ) {
        if (state.passengerinfofull[j].id == id) {
          return state.passengerinfofull[j];
          break;
        } else if (j == length) {
          return false;
        }
      }
    }
  }
};
