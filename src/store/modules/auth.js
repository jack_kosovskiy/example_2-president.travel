export default {
  state: {
    user: {
      email: '',
      password: '',
      phone: '',
      id: '',
    },
    corp: {
      registration_begin: {
        country: '',
        contract_type: ''
      },
      contract_signer: {
        firstname: '',
        lastname: '',
        fathersname: '',
        position: '',
      },
      legal_address: {
        region: '',
        city: '',
        street: '',
        post_code: '',
        house: '',
        housing: '',
        office: ''
      },
      post_address: {
        region: '',
        city: '',
        street: '',
        post_code: '',
        house: '',
        housing: '',
        office: ''
      },
      bank_requisites: {
        bank_name: '',
        bik: '',
        payment_account: '',
        kor_account: ''
      },
      contact_user: {
        fio: '',
        phone: '',
        fax: '',
        email: ''
      }
    }
  },
  mutations: {
    AuthUserFirstStep(state, payload) {
      state.user.email = payload.email;
      state.user.password = payload.password;
      state.user.phone = payload.phone;
      state.user.id = payload.id;
    },
    RegCorp1(state, payload) {
      state.corp.registration_begin.country = payload.country;
      state.corp.registration_begin.contract_type = payload.contract_type;
    },
    RegCorp2(state, payload) {
      state.corp.contract_signer.firstname = payload.firstname;
      state.corp.contract_signer.lastname = payload.lastname;
      state.corp.contract_signer.fathersname = payload.fathersname;
      state.corp.contract_signer.position = payload.position;
    },
    RegCorp3(state, payload) {
      state.corp.legal_address.region = payload.region;
      state.corp.legal_address.city = payload.city;
      state.corp.legal_address.street = payload.street;
      state.corp.legal_address.post_code = payload.post_code;
      state.corp.legal_address.house = payload.house;
      state.corp.legal_address.housing = payload.housing;
      state.corp.legal_address.office = payload.office;
    },
    RegCorp4(state, payload) {
      state.corp.post_address.region = payload.region;
      state.corp.post_address.city = payload.city;
      state.corp.post_address.street = payload.street;
      state.corp.post_address.post_code = payload.post_code;
      state.corp.post_address.house = payload.house;
      state.corp.post_address.housing = payload.housing;
      state.corp.post_address.office = payload.office;
    },
    RegCorp5(state, payload) {
      state.corp.bank_requisites.bank_name = payload.bank_name;
      state.corp.bank_requisites.bik = payload.bik;
      state.corp.bank_requisites.payment_account = payload.payment_account;
      state.corp.bank_requisites.kor_account = payload.kor_account;
    },
    RegCorp6(state, payload) {
      state.corp.contact_user.fio = payload.fio;
      state.corp.contact_user.phone = payload.phone;
      state.corp.contact_user.fax = payload.fax;
      state.corp.contact_user.email = payload.email;
    }

  },
  actions: {

  },
  getters: {
    getCorp: (state, getters) => () => {
      return state.corp
    },
  }
}
