export default {
  state: {
    passengers: [],
    passengersForMainPage: [],
  },
  mutations: {
    removePassenger(state, id) {
    	console.log(id)
    	state.passengers = state.passengers.filter(passenger => passenger.travelerId !== id);
    },
    setPassengers(state, payload) {
		state.passengers = payload
    },
    setPassengersForMainPage(state, payload) {
		state.passengersForMainPage = payload
    },
    updateTravelerInfo(state, payload) {
      var id = payload.id;
      var data = payload.data;
      for (let index = 0; index < state.passengers.length; index++) {
        const element = state.passengers[index];
        if (element.travelerId == payload.id) {
          Object.assign(element, data)
          element['loadedDetailInfo'] = true;
        }
      }
    },
    addNewTraveler(state, payload) {
      state.passengers.push(payload)
    }

  },
  actions: {

  },
  getters: {
    getFullInfoAboutPassangers: (state, getters) => (id) => {
      for (let index = 0; index < state.passengers.length; index++) {
        const element = state.passengers[index];
        if (element.travelerId == id) {
          return element;
          break;
        }
      }
    },
    getFirstName: (state, getters) => (id) => {
      for (let index = 0; index < state.passengers.length; index++) {
        const element = state.passengers[index];
        if (element.travelerId == id) {
          return element.firstname == undefined ? '' : element.firstname;
          break;
        }
      }
    },
    getLastName: (state, getters) => (id) => {
      for (let index = 0; index < state.passengers.length; index++) {
        const element = state.passengers[index];
        if (element.travelerId == id) {
          return element.lastname == undefined ? '' : element.lastname;
          break;
        }
      }
    }
  }
}
