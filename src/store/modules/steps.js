export default {
  state: {
    // isInformationalPanelShow: true,
    isFrom: true,
    isTo: false,
    isDatePicker: false,
    isPassengerPicker: false,
    isRoundTrip: true
  },
  mutations: {
    hideAll(state, payload) {
      state.isPassengerPicker = false;
      state.isDatePicker = false;
      state.isTo = false;
      state.isFrom = false;
    },
    setIsRoundTrip(state, payload) {
      state.isRoundTrip = payload;
    },
    setIsFrom(state, payload) {
      state.isFrom = payload;
      if (payload) {
        state.isPassengerPicker = false;
        state.isDatePicker = false;
        state.isTo = false;
      }
    },
    setIsTo(state, payload) {
      state.isTo = payload;
      if (payload) {
        state.isPassengerPicker = false;
        state.isDatePicker = false;
        state.isFrom = false;
      }
    },
    setIsDatePicker(state, payload) {
      state.isDatePicker = payload;
      if (payload) {
        state.isPassengerPicker = false;
        state.isTo = false;
        state.isFrom = false;
      }
    },
    setIsPassengerPicker(state, payload) {
      state.isPassengerPicker = payload;
      if (payload) {
        state.isDatePicker = false;
        state.isTo = false;
        state.isFrom = false;
      }
    }
  },
  actions: {},
  getters: {}
};
