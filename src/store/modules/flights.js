import Vue from 'vue'
export default {
  	state: {
  		selectedFlights: [],
	  	isRoundTrip: true,
	  	classId: -1,
	  	searchId: '',
	  	flightsList: [],
	  	passengersForFlight: [],
	  	contractsInfo: {}
  	},
  	mutations: {
    	setPassengersForFlight(state, payload) {
      		state.passengersForFlight = payload;
    	},
    	setSearchId(state, payload) {
      		state.searchId = payload;
    	},
		setClassId(state, payload) {
      		state.classId = payload;
    	},
    	changeRoundTrip(state, payload) {
      		state.isRoundTrip = payload;
    	},
    	addFlight(state, payload) {
      		state.selectedFlights = [];
      		state.selectedFlights.push(payload);
    	},
    	loadFlight(state, payload) {
      		state.flightsList = payload;
    	},
    	updateFlight(state, {id, policy}) {
      		for (let index = 0; index < state.flightsList.length; index++) {
      			const element = state.flightsList[index];
        		if (element.frId === id) {
          			element.loadedInfo = true;
          			Object.assign(element, policy)
        		}
      		}
    	},
    	// selectCurrency(state, payload){
    	//   state.selectedCurrency = payload;
    	// }
		addContract(state, {searchId: id, tripHash: hash, data: payload}) {
			Vue.set(state.contractsInfo, `${id}_${hash}`, payload)
		}
  	},
  	actions: {},
  	getters: {
    	getSelectedPass: (state, getters) => () => {
      		return state.passengersForFlight;
    	},
    	getSearchId: (state, getters) => () => {
      		return state.searchId
    	},
    	getList: (state, getters) => () => {
      		return state.list
    	},
		getIsRound: (state, getters) => () => {
      		return state.isRoundTrip
    	},
		getCurrentContractInfo: (state) => {
    	  if (state.selectedFlights.length) return state.contractsInfo[`${state.searchId}_${state.selectedFlights[0].tripHash}`];
    	  else return undefined
    	}
  	}
}
