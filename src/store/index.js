import Vue from "vue";
import Vuex from "vuex";
import preloader from "./modules/preloader";
import langs from "./modules/langs";
import currencies from "./modules/currencies";
import searchparameters from "./modules/searchparameters";
import flights from "./modules/flights";
import airports from "./modules/airports";
import params from "./modules/params";
import passengers from "./modules/passengers";
import hotels from "./modules/hotels";
import steps from "./modules/steps";
import auth from "./modules/auth";
import passengerinfofull from "./modules/passangerInfoFull";
import events from "./modules/events";
Vue.use(Vuex);
const modules = {
	preloader,
	airports,
	langs,
	currencies,
	flights,
	searchparameters,
	params,
	passengers,
	hotels,
	steps,
	auth,
	passengerinfofull,
	events
};

export default new Vuex.Store({
  state: {},
  mutations: {
  	clear (state) {
		for (let module in state) {
			if (state.hasOwnProperty(module)) {
				let initialState = modules[module];
				console.log(modules.hasOwnProperty(module), module)
				Vue.set(state, module, initialState.state);
			}
		}
	}
  },
  modules
});
