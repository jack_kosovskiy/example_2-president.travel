import axios from "axios";
import createUrlQuery from "./utils/createUrlQuery"
const baseURL = typeof customConfig !== "undefined" && customConfig.hasOwnProperty("urlAPI")
	? customConfig.urlAPI : '//api.president.travel';
export const HTTP = axios.create({
	baseURL
});

export const API = {
	config: {
		data: {
			lang: '',
			currency: '',
		}
	},
	init: function () {
		return HTTP.post("/init/index", null, API.config);
	},
	auth: function (user) {
		return HTTP.post("/user/authorize", user);
	},
	regCorporate: function (data) {
		return HTTP.post("/company/registration/register", data)
	},
	validateCorporate: function (data) {
		return HTTP.post("/company/registration/validate", data);
	},
	regUser: function (user) {
		return HTTP.post("/user/register", user);
	},
	confirmSmsCode: function (code) {
		return HTTP.post("/user/check-sms-auth-key", code);
	},
	reSendSmsCode: function (phone) {
		return HTTP.post("/user/get-sms-code", phone);
	},
	resetPass: function (email) {
		return HTTP.post("/user/password/initreset", email);
	},
	travelerUpdate: function (traveler) {
		return HTTP.post("/traveler/update", traveler, API.config);
	},
	travelerCreate: function (traveler) {
		return HTTP.post("/traveler/create", traveler, API.config);
	},
	getTrevelersInfo: function (traveler) {
		return HTTP.post("/traveler/search", traveler, API.config);
	},
	getAllPreferences: function (data) {
		return HTTP.post("/traveler/preferences/info", data, API.config)
	},
	getAllHistory: function (data) {
		return HTTP.post("/traveler/orders", data, API.config)
	},
	setTrevelerCitizenship: function (data) {
		return HTTP.post("/traveler/citizenship/update", data, API.config)
	},
	updateTravelerPassport: function (data) {
		return HTTP.post("/traveler/document/update", data, API.config)
	},
	createPreferenceCard: function (data) {
		return HTTP.post("/traveler/preferences/card/create", data, API.config)
	},
	updatePreferenceCard: function (data) {
		return HTTP.post("/traveler/preferences/card/update", data, API.config)
	},
	deletePreferenceCard: function (data) {
		return HTTP.post("/traveler/preferences/card/delete", data, API.config)
	},
	companyUpdate: function (data) {
		return HTTP.post("/company/update-requisites", data, API.config)
	},
	getCities: function (data, options = {}) {
		return HTTP.post("/city/search", data, Object.assign(options, API.config));
	},
	getFlights: function (search) {
		return HTTP.post("/ticket/avia/search", search, API.config);
	},
	filterTickets: function (filterParam) {
		return HTTP.post("/ticket/avia/filter", filterParam, API.config);
	},
	getContractInfo (data) {
		return HTTP.post("/ticket/avia/contract", data, API.config);
	},
	getOrder: function (data) {
		return HTTP.post("/order/get/by-date", data, API.config);
	},
	buyFlight: function (data) {
		return HTTP.post("/avia/order/book", data, API.config);
	},
	searchAirline: function (data) {
		return HTTP.post("/search/airline", data, API.config)
	},
	createAirline: function (data) {
		return HTTP.post("/traveler/preferences/airline/create", data, API.config)
	},
	updateAirline: function (data) {
		return HTTP.post("/traveler/preferences/airline/update", data, API.config)
	},
	deleteAirline: function (data) {
		return HTTP.post("/traveler/preferences/airline/delete", data, API.config)
	},
	searchAirport: function (data) {
		return HTTP.post("/search/airport", data, API.config)
	},
	createAirport: function (data) {
		return HTTP.post("/traveler/preferences/airport/create", data, API.config)
	},
	deleteAirport: function (data) {
		return HTTP.post("/traveler/preferences/airport/delete", data, API.config)
	},
	updateAirport: function (data) {
		return HTTP.post("/traveler/preferences/airport/update", data, API.config)
	},
	getUserInfo: function (data) {
		return HTTP.post("/user/info", data, API.config)
	},
	uptadePersonalInfo: function (user) {
		return HTTP.post("/user/update", user, API.config);
	},
	exportCashback: function (data) {
		return HTTP.post("/user/export-cashback", data, API.config)
	},
	getCompanyInfo: function (data) {
		return HTTP.post("/company/info", data, API.config)
	},
	incDeposit: function (data) {
		return HTTP.post("/company/increase-deposit", data, API.config)
	},
	companyUpdate: function (data) {
		return HTTP.post("/company/update-requisites", data, API.config)
	},
	getFlightInfo: function (data) {
		return HTTP.post("avia/order/tariffs", data, API.config)
	},
	removeTraveler: function (data) {
		return HTTP.post("/traveler/delete", data, API.config)
	},
	changePassword: function (data) {
		return HTTP.post("/user/change-password", data, API.config)
	},
	getHotelCity: function (data, config = {}) {
		return HTTP.post("/hotel/suggest", data, Object.assign(config, API.config))
	},
	searchHotel: function (data) {
		return HTTP.post("/hotel/search", data, API.config)
	},
	hotelFilter: function (data) {
		return HTTP.post('/hotel/filter', data, API.config)
	},
	getRoomsInfo: function (data) {
		return HTTP.post("/hotel/live-rates", data, API.config)
	},
	getHotelInfo (data) {
		return HTTP.post("/hotel/info", data, API.config)
	},
	getHotelContract: function (data) {
		return HTTP.post("/hotel/contract", data, API.config)
	},
	reserveHotel: function (data) {
		return HTTP.post("/hotel/book", data, API.config)
	},
	getHotelPhotos: function (data) {
		return HTTP.post("/hotel/images", data, API.config)
	},
	hotelBookInfo (data) {
		return HTTP.post("/hotel/book-details", data, API.config)
	},
	ticketBookInfo (data) {
		return HTTP.post("/avia/order/status", data, API.config)
	},
	orderStatus (data) {
		return HTTP.post(`/order/status?${createUrlQuery(data)}`, {}, API.config)
	}
};
